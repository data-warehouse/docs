# Edumall Data Warehouse

> ### Table Of Contents
>
> [Concept](#concept)<br>
> [Requirement](#requirement)<br>
> [Architecture](#architecture)<br>
> [Data Model](#data-model)<br>
> [Analysis](#analysis)<br>
> [Design](#design)<br>
> [Build](#build)<br>
> [Test](#test)<br>
> [Implementation](#implementation)<br>
> [Project Management](#project-management)
>

## Concept

- Business concept for DW
- Overiew architecture for enterprice DW

## Requirement

- Business Requirement
- Data Requirement
- Query Requirement
- Technical Requirement
- Interface Requirement
- Business Definitions Dictionary

## Architecture

- [Technical Architecture](../holistics-etl/README.md)
- Security Model
- Resilience Plan
- Data Quality Plan

## Data Model

- Data Model Standard
- Logical Model
- [Repository Data Model](../holistics-etl/T-ransforms/repository-data-model.md)
- [Data Mart Data Model](../holistics-etl/T-ransforms/data-mart.md)

## Analysis

- Source Systems Analysis
- Data Profiling
- Source Entity Analysis
- Target Orientated Analysis

## Design

- ETL Excution Plan
- Initial Capacity Plan
- Code Standards

## Build

- Code Repository
- Data Cleansing Intergration

## Test

- Unit Testing
- System Testing
- Intergration Testing
- Performance Testing

## Implementation

- Configuration Management Procedures
- Operations Guide
- Capacity Plan
- Service Level Agreements (SLA)
- Helpdesk Scripts
- Training Plan
- Operational Schedule
- System Monitoring Plan

## Project Management

- [Documentation Roadmap](../documentations/documentation_roadmap.md)
- Project Plan
- 'DRIVE' Statements
- 'SWOT' Analysis
- 'MoSCoW' Analysis
- Change Requests (CR)
- Risk Register
- Issue/Bug Log
- Key Design Decisions (KDD)