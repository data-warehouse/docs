# Metabase

The open-source bussiness intelligence tool for analysis OLAP database

* Homepage: http://metabase.com
* Github repo: https://github.com/metabase/metabase
* Docker image: https://hub.docker.com/r/metabase/metabase/

## :warning: DEPRECATED

**This repo is no longer maintenance, please use [Metabase helm chart](https://github.com/kubernetes/charts/stable/metabase) instead**

## Notes

**Current limitation we should aware**

* Currently Metabase v0.24.2 is not horizontly scalable
