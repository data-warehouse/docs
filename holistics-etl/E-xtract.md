# E-xtract: extract data from data sources into data lake

## Descriptions

1. Data sources(OLTP databases): such as bifrost, marol, nami, medusa, contactc3, gambit, ...

2. Data lake:

    * Type: Redshift

    * Schema: lake

## Implementation

1. Specify data sources: https://secure.holistics.io/manage#/data_sources

2. Using Holistics [Web UI](https://secure.holistics.io/data_imports) to define import jobs

    | Data sources | Table/Collection | Cleansing logic                              | Data lake                |
    |--------------|------------------|----------------------------------------------|--------------------------|
    | bifrost      |                  |                                              |                          |
    |              | transactions     |                                              | lake.transactions        |
    | marol        |                  |                                              |                          |
    |              | courses          |                                              | bcourses                 |
    |              | orders           | flag_deleted IS NULL OR flag_deleted <> TRUE | lake.morders             |
    |              | history_orders   | flag_deleted IS NULL OR flag_deleted <> TRUE | lake.history_orders      |
    |              | staffs           |                                              | lake.telesellers         |
    |              | call_logs        |                                              | lake.call_logs           |
    |              | exchange_orders  |                                              | lake.exchange_orders     |
    | nami         |                  |                                              |                          |
    |              | ads              |                                              | lake.ads                 |
    |              | ad_bydates       |                                              | lake.ad_bydates          |
    |              | channel_courses  |                                              | lake.channel_courses     |
    |              | adaccounts       |                                              | lake.adaccounts          |
    |              | channels         |                                              | lake.channels            |
    |              | users            |                                              | lake.marketers           |
    |              | ccusers          |                                              | lake.ccusers             |
    |              | campaigns        |                                              | lake.campaigns           |
    |              | ad_groups        |                                              | lake.ad_groups           |
    |              | c3copies         |                                              | lake.c3copies            |
    |              | groups           |                                              | lake.mar_teams           |
    |              | group_users      |                                              | lake.mar_teams_marketers |
    |              | course_rates     |                                              | lake.course_rates        |
    | medusa       |                  |                                              |                          |
    |              | landingpage      |                                              | lake.landingpage         |
    | contactc3    |                  |                                              |                          |
    |              | contactc3s       |                                              | lake.contactc3s          |
    | gambit       |                  |                                              |                          |
    |              | logs             |                                              | lake.glogs               |
    |              | users            |                                              | lake.gusers              |
    |              | partners         |                                              | lake.parcel_partners     |


3. Manually trigger import jobs or set schedule time, currently we are scheduled these jobs to perfom every day at 1AM UTC+0700

## Important note

> Currently Holistics doesn't support support import from static SQL / JSON file
> So, we had to manually import provinces data by running bellow query:

```sql
/*
-- E-xtract: provinces
*/

CREATE TABLE IF NOT EXISTS lake.provinces (
    code varchar(255) PRIMARY KEY,
    name varchar(255),
    region varchar(255),
    nation varchar(255)
);

TRUNCATE TABLE lake.provinces;

INSERT INTO lake.provinces(code, name, region, nation) VALUES('AGG', 'An Giang', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('BDG', 'Bình Dương', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('BDH', 'Bình Định', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('BGG', 'Bắc Giang', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('BKN', 'Bắc Kạn', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('BLU', 'Bạc Liêu', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('BNH', 'Bắc Ninh', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('BPC', 'Bình Phước', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('BTE', 'Bến Tre', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('BTN', 'Bình Thuận', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('CBG', 'Cao Bằng', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('CMU', 'Cà Mau', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('CTO', 'Cần Thơ', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('DBN', 'Điện Biên', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('DKG', 'Đăk Nông', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('DLK', 'Ðắk Lắk', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('DNG', 'Đà Nẵng', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('DNI', 'Đồng Nai', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('DTP', 'Đồng Tháp', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('GLI', 'Gia Lai', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('HBH', 'Hòa Bình', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('HCM', 'Hồ Chí Minh', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('HDG', 'Hải Dương', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('HGG', 'Hà Giang', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('HNI', 'Hà Nội', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('HNM', 'Hà Nam', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('HPG', 'Hải Phòng', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('HTH', 'Hà Tĩnh', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('HUE', 'Thừa Thiên - Huế', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('HUG', 'Hậu Giang', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('HYN', 'Hưng Yên', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('KGG', 'Kiên Giang', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('KHA', 'Khánh Hòa', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('KTM', 'Kon Tum', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('LAN', 'Long An', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('LCI', 'Lào Cai', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('LCU', 'Lai Châu', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('LDG', 'Lâm Đồng', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('LSN', 'Lạng Sơn', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('NAN', 'Nghệ An', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('NBH', 'Ninh Bình', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('NDH', 'Nam Định', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('NTN', 'Ninh Thuận', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('PHO', 'Phú Thọ', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('PYN', 'Phú Yên', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('QBH', 'Quảng Bình', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('QNH', 'Quảng Ninh', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('QNI', 'Quảng Ngãi', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('QNM', 'Quảng Nam', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('QTI', 'Quảng Trị', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('SLA', 'Sơn La', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('STG', 'Sóc Trăng', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('TBH', 'Thái Bình', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('TGG', 'Tiền Giang', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('THA', 'Thanh Hóa', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('TNH', 'Tây Ninh', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('TNN', 'Thái Nguyên', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('TQG', 'Tuyên Quang', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('TVH', 'Trà Vinh', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('VLG', 'Vĩnh Long', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('VPC', 'Vĩnh Phúc', 'Miền Bắc', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('VTU', 'Bà Rịa - Vũng Tàu', 'Miền Nam', 'Việt Nam');
INSERT INTO lake.provinces(code, name, region, nation) VALUES('YBN', 'Yên Bái', 'Miền Bắc', 'Việt Nam');
```
