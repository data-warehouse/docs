# Data Mart Data Model

Detail Data Model: [Holistic Data Transform](https://secure.holistics.io/data_transforms)

Schema Type: Star Schema

| **Table Type**              | **Data Mart Table**                                          | **Description**                                          |
| --------------------------- | ------------------------------------------------------------ | -------------------------------------------------------- |
| **Fact Table**              |                                                              |                                                          |
|                             | [warehouse.lead_facts](../holistics-etl/T-ransforms/warehouse.lead_facts.sql.md) | bảng fact                                                |
| **Staging Dimention Table** |                                                              |                                                          |
|                             | [staging.eros_dms](../holistics-etl/T-ransforms/staging.eros_dms.sql.md) | chiều sale                                               |
|                             | [staging.mar_dms](../holistics-etl/T-ransforms/staging.mar_dms.sql.md) | chiều marketing                                          |
|                             | [staging.bifrost_dms](../holistics-etl/T-ransforms/staging.bifrost_dms.sql.md) | chiều thanh toán (không sale - marketing)                |
| **Dimention Table**         |                                                              |                                                          |
|                             | [warehouse.ads_dms](../holistics-etl/T-ransforms/warehouse.ads_dms.sql.md) | chiều quảng cáo                                          |
|                             | [warehouse.course_dms](../holistics-etl/T-ransforms/warehouse.course_dms.sql.md) | chiều khóa học                                           |
|                             | [warehouse.customer_dms](../holistics-etl/T-ransforms/warehouse.customer_dms.sql.md) | chiều khách hàng                                         |
|                             | [warehouse.date_dms](../holistics-etl/T-ransforms/warehouse.date_dms.sql.md) | chiều thời gian đăng ký contact                          |
|                             | [warehouse.lading_date_dms](../holistics-etl/T-ransforms/warehouse.lading_date_dms.sql.md) | chiều thời gian giao vận                                 |
|                             | [warehouse.lading_dms](../holistics-etl/T-ransforms/warehouse.lading_dms.sql.md) | chiều đối tác giao vận                                   |
|                             | [warehouse.lading_employee_dms](../holistics-etl/T-ransforms/warehouse.lading_employee_dms.sql.md) | chiều nhân viên hệ thống psmo                            |
|                             | [warehouse.level_dms](../holistics-etl/T-ransforms/warehouse.level_dms.sql.md) | chiều level đơn hàng                                     |
|                             | [warehouse.order_date_dms](../holistics-etl/T-ransforms/warehouse.order_date_dms.sql.md) | chiều thời gian đơn hàng                                 |
|                             | [warehouse.order_sale_dms](../holistics-etl/T-ransforms/warehouse.order_sale_dms.sql.md) | chiều nhân viên chăm sóc đơn hàng                        |
|                             | [warehouse.payment_checker_dms](../holistics-etl/T-ransforms/warehouse.payment_checker_dms.sql.md) | chiều thông tin đối soát                                 |
|                             | [warehouse.payment_dms](../holistics-etl/T-ransforms/warehouse.payment_dms.sql.md) | chiều thông tin giao dịch                                |
|                             | [warehouse.product_dms](../holistics-etl/T-ransforms/warehouse.product_dms.sql.md) | chiều sản phẩm                                           |
|                             | [warehouse.telemar_dms](../holistics-etl/T-ransforms/warehouse.telemar_dms.sql.md) | chiều nhân viên chăm sóc đơn hàng từ L1 -> L3 (telemar)  |
|                             | [warehouse.telesale_dms](../holistics-etl/T-ransforms/warehouse.telesale_dms.sql.md) | chiều nhân viên chăm sóc đơn hàng từ L3 -> L8 (telesale) |

