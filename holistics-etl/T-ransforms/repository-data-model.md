# Repository Data Model

Detail Data Model: [Holistic Data Imports](https://secure.holistics.io/data_imports)

| **Data type**          | **Data-sources**      | **Database Type** | **Table/Collection** | **Cleansing Logic**                          | **Data Lake/ Repository Data Model** | Data Lake Type |
| ---------------------- | --------------------- | ----------------- | -------------------- | -------------------------------------------- | ------------------------------------ | -------------- |
| **DATABASE**           |                       |                   |                      |                                              |                                      |                |
|                        | **bifrost**           | MySQL             |                      |                                              |                                      | Redshift       |
|                        |                       |                   | transactions         |                                              | lake.transactions                    |                |
|                        |                       |                   | courses              |                                              | lake.bcourses                        |                |
|                        |                       |                   | buyers               |                                              | lake.bbuyers                         |                |
|                        |                       |                   | xcheck_transactions  |                                              | lake.xcheck_transactions             |                |
|                        |                       |                   | transaction_details  |                                              | lake.transaction_details             |                |
|                        | **marol**             | MongoDB           |                      |                                              |                                      | Redshift       |
|                        |                       |                   | courses              |                                              | lake.bcourses                        |                |
|                        |                       |                   | orders               | flag_deleted IS NULL OR flag_deleted <> TRUE | lake.morders                         |                |
|                        |                       |                   | history_orders       | flag_deleted IS NULL OR flag_deleted <> TRUE | lake.history_orders                  |                |
|                        |                       |                   | staffs               |                                              | lake.telesellers                     |                |
|                        |                       |                   | call_logs            |                                              | lake.call_logs                       |                |
|                        |                       |                   | exchange_orders      |                                              | lake.exchange_orders                 |                |
|                        |                       |                   | group_works          |                                              | lake.group_works                     |                |
|                        |                       |                   | exchange_orders      |                                              | lake.exchange_orders                 |                |
|                        | **nami**              | PostgreSQL        |                      |                                              |                                      | Redshift       |
|                        |                       |                   | ads                  |                                              | lake.ads                             |                |
|                        |                       |                   | ad_bydates           |                                              | lake.ad_bydates                      |                |
|                        |                       |                   | channel_courses      |                                              | lake.channel_courses                 |                |
|                        |                       |                   | adaccounts           |                                              | lake.adaccounts                      |                |
|                        |                       |                   | channels             |                                              | lake.channels                        |                |
|                        |                       |                   | users                |                                              | lake.marketers                       |                |
|                        |                       |                   | channel_course_users |                                              | lake.channel_course_users            |                |
|                        |                       |                   | campaigns            |                                              | lake.campaigns                       |                |
|                        |                       |                   | ad_groups            |                                              | lake.ad_groups                       |                |
|                        |                       |                   | c3copies             |                                              | lake.c3copies                        |                |
|                        |                       |                   | groups               |                                              | lake.mar_teams                       |                |
|                        |                       |                   | group_users          |                                              | lake.mar_teams_marketers             |                |
|                        |                       |                   | course_rates         |                                              | lake.course_rates                    |                |
|                        |                       |                   | course_categories    |                                              | lake.mar_categories                  |                |
|                        |                       |                   | departments          |                                              | lake.mar_departments                 |                |
|                        |                       |                   | courses              |                                              | lake.mar_courses                     |                |
|                        | **medusa**            | MongoDB           |                      |                                              |                                      | Redshift       |
|                        |                       |                   | landingpage          |                                              | lake.landingpage                     |                |
|                        | **contactc3**         | MongoDB           |                      |                                              |                                      | Redshift       |
|                        |                       |                   | contactc3s           |                                              | lake.contactc3s                      |                |
|                        | **gambit**            | MongoDB           |                      |                                              |                                      | Redshift       |
|                        |                       |                   | logs                 |                                              | lake.glogs                           |                |
|                        |                       |                   | users                |                                              | lake.gusers                          |                |
|                        |                       |                   | partners             |                                              | lake.parcel_partners                 |                |
|                        |                       |                   | orders               |                                              | lake.gorders                         |                |
|                        |                       |                   | order_courses        |                                              | lake.gchild_orders                   |                |
|                        | **jackfruit**         | MongoDB           |                      |                                              |                                      | Redshift       |
|                        |                       |                   | primary_categories   |                                              | lake.primary_categories              |                |
|                        |                       |                   | payments             |                                              | lake.edumall_payments                |                |
|                        |                       |                   | courses              |                                              | lake.courses                         |                |
|                        |                       |                   | sale_packages        |                                              | lake.sale_packages                   |                |
|                        |                       |                   | categories           |                                              | lake.categories                      |                |
| **GOOGLE SPREADSHEET** |                       |                   |                      |                                              |                                      | Redshift       |
|                        | https://goo.gl/5bBnS2 |                   | team_plan_revenue    |                                              | lake.team_plan_revenue               |                |
|                        | https://goo.gl/ZDASBF |                   | plan_by_day          |                                              | lake.plan_by_day                     |                |
|                        | https://goo.gl/MkK3Qg |                   | import_sale          |                                              | lake.employee_sale                   |                |



