/* staging.bifrost_dms - Update 11/07/2018 by Tunnm  */

SELECT
    ('dir_' || ISNULL(CAST(tr.buyer_id AS VARCHAR), 'unknown') || '.' || ISNULL(CAST(bcourses.id AS VARCHAR), 'unknown')) AS transaction_course_id,
    tr.id                                                                                 AS id,
    tr.method                                                                             AS method,
    tr.price                                                                              AS price,
    tr.credit                                                                             AS credit,
    tr.status                                                                             AS status,
    tr.buyer_id                                                                           AS buyer_id,
    CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, tr.created_at))               AS created_at,
    tr.cost                                                                               AS cost,
    tr.seller                                                                             AS seller,
    tr.combo                                                                              AS combo,
    tr.source                                                                             AS source,
    tr.source_url                                                                         AS source_url,
    tr.siren_id                                                                           AS siren_id,
    CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, tr.received_at))              AS received_at,
    CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, tr.updated_at))               AS updated_at,
    tr.note                                                                               AS note,
    tr.orderer_email                                                                      AS orderer_email,
    tr.transfer_code                                                                      AS transfer_code,
    tr.cod_code                                                                           AS cod_code,
    tr.coupon_code                                                                        AS coupon_code,
    tr.edumall_payment_id                                                                 AS edumall_payment_id,
    (CASE   WHEN tr.status = 'success'    THEN    1
            WHEN tr.status = 'pending'    THEN    2
            WHEN tr.status = 'failed'     THEN    3
            WHEN tr.status = 'canceled'   THEN    4
            ELSE 5
    END)                                                                                  AS status_number,
    ROW_NUMBER() OVER (PARTITION BY tr.buyer_id, bcourses.id ORDER BY status_number ASC)  AS status_ranked,
    ROW_NUMBER() OVER (PARTITION BY tr.buyer_id, bcourses.id ORDER BY tr.created_at DESC) AS ranked
    FROM lake.transactions AS tr
    LEFT JOIN lake.bcourses ON bcourses._transaction_id = tr.id
    WHERE tr.id IS NOT NULL AND (
    (UPPER(tr.method) = 'COD' AND tr.siren_id IS NULL) OR
    (UPPER(tr.method) = 'GATEWAY' AND tr.siren_id IS NULL) OR
    -- Add if gateway has siren_id
    -- (UPPER(tr.method) = 'GATEWAY' AND UPPER(tr.source) = 'EOP_GATEWAY') OR
    (UPPER(tr.method) = 'CASH'    AND tr.siren_id IS NULL) OR
    (UPPER(tr.method) = 'TRANSFER'    AND tr.siren_id IS NULL) OR
    (UPPER(tr.source) = 'B2B') OR
    (UPPER(tr.source) = 'CHANNEL_ECOMMERCE')
)