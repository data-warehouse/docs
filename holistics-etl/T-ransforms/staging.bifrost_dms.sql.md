# [staging.bifrost_dms](../holistics-etl/T-ransforms/staging.bifrost_dms.sql)

| **Field** | **Data Type** | **Table Lake** | **Description** | **Note** |
| --------- | ------------- | -------------- | --------------- | -------- |
| transaction_course_id | varchar(27) (NOT NULL) | lake.transactions<br>lake.bcourses | id transaction courses | 1, 4 |
| id | int4 | lake.transactions | id transaction | 2, 4 |
| method | varchar(1020) | lake.transactions | phương thức thanh toán | 4 |
| price | float4 | lake.transactions | giá bán | 4 |
| credit | int4 | lake.transactions | tín dụng | 4 |
| status | varchar(1020) | lake.transactions | trạng thái thanh toán | 4 |
| buyer_id | int4 | lake.transactions | buyer_id | 4 |
| created_at | timestamp (NOT NULL) | lake.transactions | ngày tạo | 3, 4 |
| cost | int4 | lake.transactions | giá cả | 4 |
| seller | varchar(1020) | lake.transactions | người bán | 4 |
| combo | varchar(1020) | lake.transactions | combo | 4 |
| source | varchar(1020) | lake.transactions | nguồn | 4 |
| source_url | varchar(8332) | lake.transactions | nguồn url | 4 |
| siren_id | varchar(1020) | lake.transactions | siren_id | 4 |
| received_at | timestamp (NOT NULL) | lake.transactions | ngày khách nhận hàng | 3, 4 |
| updated_at | timestamp (NOT NULL) | lake.transactions | ngày cập nhật | 3, 4 |
| note | varchar(8332) | lake.transactions | thông tin thanh toán bổ sung | 4 |
| orderer_email | varchar(8332) | lake.transactions | người đặt hàng (saler) | 4 |
| transfer_code | varchar(255) | lake.transactions | mã chuyển khoản | 4 |
| cod_code | varchar(1020) | lake.transactions | mã cod | 4 |
| coupon_code | varchar(1020) | lake.transactions | mã coupon | 4 |
| edumall_payment_id | varchar(255) | lake.transactions | edumall_payment_id | 4 |
| status_number | int4 | lake.transactions | status_number | 4, 5 |
| status_ranked | int8 | lake.transactions<br>lake.bcourses | status_ranked | 4, 6 |
| ranked | int8 | lake.transactions<br>lake.bcourses | ranked | 4, 7 |


**Note:**

1. *transaction_course_id* của *bifrost_dms* có cấu trúc: *'dir_' + lake.transactions.buyer_id + lake.bcourses.id*
2. *id* của *bifrost_dms* được định nghĩa là id bảng *lake.transactions*
3. timezone : *Asia/Ho_Chi_Minh*
4. staging.bifrost_dms được định nghĩa là chiều thông tin thanh toán không qua sale và marketing, được định nghĩa bằng logic sau:

```sql
FROM lake.transactions
WHERE transactions.id IS NOT NULL AND (
(transactions.method = 'COD'      AND transactions.siren_id IS NULL)   OR
(transactions.method = 'GATEWAY'  AND transactions.siren_id IS NULL)   OR
(transactions.method = 'CASH'     AND transactions.siren_id IS NULL)   OR
(transactions.method = 'TRANSFER' AND transactions.siren_id IS NULL)   OR
(transactions.source = 'B2B')     OR
(transactions.source = 'CHANNEL_ECOMMERCE'))
```
5. *status_number* được định nghĩa là giá trị chuyển đổi trạng thái thanh toán theo số với logic:

```sql
(CASE
  WHEN transactions.status = 'success'    THEN    1
  WHEN transactions.status = 'pending'    THEN    2
  WHEN transactions.status = 'failed'     THEN    3
  WHEN transactions.status = 'canceled'   THEN    4
  ELSE 											  5
END)
```
6. *status_ranked* là xếp hạng cho giao dịch theo *status_number* theo thứ tự ưu tiên về trạng thái thanh toán của đơn hàng: *success -> pending -> failed -> canceled*
7. *ranked* là xếp hạng cho giao dịch theo *created_at* theo thứ tự giảm dần của *created_at*

