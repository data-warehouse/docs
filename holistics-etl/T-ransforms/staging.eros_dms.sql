/* staging.eros_dms - Update 15:00PM 02/05/2018 by Tunnm  */

WITH gorder_extended AS (
  (
    SELECT
      gorders._id AS _id,
      gorders.siren_id AS siren_id,
      gorders.lading_price AS lading_price,
      gorders.lading_fee AS lading_fee,
      gorders.user_id AS user_id,
      gorders.s_level AS s_level,
      gorders.created_at AS created_at
    FROM lake.gorders
  )
  UNION
  (
    SELECT
      gchild_orders._id AS _id,
      gchild_orders.siren_id AS siren_id,
      gorders.lading_price AS lading_price,
      gorders.lading_fee AS lading_fee,
      gorders.user_id AS user_id,
      gorders.s_level AS s_level,
      gorders.created_at AS created_at
    FROM lake.gchild_orders
    LEFT JOIN lake.gorders ON gorders._id = gchild_orders.order_id
  )
),
gorders_ranked_transactions AS (
  SELECT
    gorder_extended._id AS _id,
    gorder_extended.siren_id AS siren_id,
    gorder_extended.lading_price AS lading_price,
    gorder_extended.lading_fee AS lading_fee,
    transactions.price AS transaction_price,
    gorder_extended.user_id AS user_id,
    transactions.id AS transaction_id,
    CONVERT_TIMEZONE(
      'Asia/Ho_Chi_Minh',
      CONVERT(TIMESTAMP, gorder_extended.created_at)
    ) AS created_at,
    -- ROW_NUMBER() OVER (PARTITION BY gorders.siren_id ORDER BY gorders.created_at DESC) AS ranked
    (
      CASE WHEN transactions.status = 'success' THEN 1 WHEN transactions.status = 'pending' THEN 2 WHEN transactions.status = 'failed' THEN 3 WHEN transactions.status = 'canceled' THEN 4 ELSE 5 END
    ) AS status_ranked,
    SUBSTRING(
      gorder_extended.s_level
      FROM
        1 FOR 2
    ) AS slevel_ranked,
    ROW_NUMBER() OVER (
      PARTITION BY gorder_extended.siren_id
      ORDER BY
        status_ranked ASC,
        slevel_ranked DESC
    ) AS ranked,
    (
      CASE WHEN transactions.status = 'success' THEN CONVERT_TIMEZONE(
        'Asia/Ho_Chi_Minh',
        CONVERT(TIMESTAMP, transactions.received_at)
      ) END
    ) AS received_at
    -- transactions.edumall_payment_id
  FROM
    gorder_extended
    LEFT JOIN lake.transactions ON transactions.siren_id = gorder_extended.siren_id
),
transactions_ranked AS (
  SELECT
    id,
    siren_id,
    method,
    price,
    -- ROW_NUMBER() OVER (PARTITION BY transactions.siren_id ORDER BY transactions.created_at DESC) AS ranked
    (
      CASE WHEN transactions.status = 'success' THEN 1 WHEN transactions.status = 'pending' THEN 2 WHEN transactions.status = 'failed' THEN 3 WHEN transactions.status = 'canceled' THEN 4 ELSE 5 END
    ) AS status_ranked,
    ROW_NUMBER() OVER (
      PARTITION BY transactions.siren_id
      ORDER BY
        status_ranked ASC
    ) AS ranked,
    (
      CASE WHEN transactions.status = 'success' THEN CONVERT_TIMEZONE(
        'Asia/Ho_Chi_Minh',
        CONVERT(TIMESTAMP, received_at)
      ) END
    ) AS received_at
    -- edumall_payment_id
  FROM
    lake.transactions
),
morders_stat AS (
  WITH history_orders_ranked AS (
    SELECT
      history_orders.order_id AS order_id,
      CONVERT_TIMEZONE(
        'Asia/Ho_Chi_Minh',
        CONVERT(TIMESTAMP, created_at)
      ) AS created_at,
      level,
      staff_id,
      ROW_NUMBER() OVER (
        PARTITION BY order_id,
        level
        ORDER BY
          created_at ASC
      ) AS ranked
    FROM
      lake.history_orders
  )
  SELECT
    morders._id AS _id,
    CONVERT_TIMEZONE(
      'Asia/Ho_Chi_Minh',
      CONVERT(TIMESTAMP, morders.created_at)
    ) AS created_at,
    COUNT(DISTINCT call_logs.order_id) AS call_logs_count,
    COUNT(DISTINCT gorders.siren_id) AS gorders_count,
    morders.level AS level,
    morders.staff_id AS staff_id,
    morders.course_id AS course_id,
    MAX(CASE WHEN hor.level = 'L1'  THEN hor.staff_id END)    AS l1_staff_id,
    MAX(CASE WHEN hor.level = 'L2'  THEN hor.staff_id END)    AS l2_staff_id,
    MAX(CASE WHEN hor.level = 'L2X' THEN hor.staff_id END)    AS l2x_staff_id,
    MAX(CASE WHEN hor.level = 'L3A' THEN hor.staff_id END)    AS l3a_staff_id,
    MAX(CASE WHEN hor.level = 'L3B' THEN hor.staff_id END)    AS l3b_staff_id,
    MAX(CASE WHEN hor.level = 'L4A' THEN hor.staff_id END)    AS l4a_staff_id,
    MAX(CASE WHEN hor.level = 'L4B' THEN hor.staff_id END)    AS l4b_staff_id,
    MAX(CASE WHEN hor.level = 'L7'  THEN hor.staff_id END)    AS l7_staff_id,
    MAX(CASE WHEN hor.level = 'L7A' THEN hor.staff_id END)    AS l7a_staff_id,
    MAX(CASE WHEN hor.level = 'L7B' THEN hor.staff_id END)    AS l7b_staff_id,
    MAX(CASE WHEN hor.level = 'L8A' THEN hor.staff_id END)    AS l8a_staff_id,
    MAX(CASE WHEN hor.level = 'L8B' THEN hor.staff_id END)    AS l8b_staff_id,
    MAX(CASE WHEN hor.level = 'L1'  THEN hor.created_at END)  AS l1_at,
    MAX(CASE WHEN hor.level = 'L2'  THEN hor.created_at END)  AS l2_at,
    MAX(CASE WHEN hor.level = 'L2X' THEN hor.created_at END)  AS l2x_at,
    MAX(CASE WHEN hor.level = 'L3A' THEN hor.created_at END)  AS l3a_at,
    MAX(CASE WHEN hor.level = 'L3B' THEN hor.created_at END)  AS l3b_at,
    MAX(CASE WHEN hor.level = 'L4A' THEN hor.created_at END)  AS l4a_at,
    MAX(CASE WHEN hor.level = 'L4B' THEN hor.created_at END)  AS l4b_at,
    MAX(CASE WHEN hor.level = 'L7A' THEN hor.created_at END)  AS l7a_at,
    MAX(CASE WHEN hor.level = 'L7B' THEN hor.created_at END)  AS l7b_at,
    MAX(CASE WHEN hor.level = 'L8A' THEN hor.created_at END)  AS l8a_at,
    MAX(CASE WHEN hor.level = 'L8B' THEN hor.created_at END)  AS l8b_at,
    LEAST(l2_at, l2x_at)                                      AS first_l2_at,
    LEAST(l3a_at, l3b_at)                                     AS first_l3_at,
    LEAST(l4a_at, l4b_at)                                     AS first_l4_at,
    LEAST(l7a_at, l7b_at)                                     AS first_l7_at,
    LEAST(l8a_at, l8b_at)                                     AS first_l8_at,
    morders.email
  FROM
    lake.morders
    LEFT JOIN lake.call_logs ON morders._id = call_logs.order_id
    LEFT JOIN history_orders_ranked AS hor ON hor.order_id = morders._id AND hor.ranked = 1
    LEFT JOIN lake.gorders ON morders._id = gorders.siren_id
  WHERE
    morders.flag_deleted IS NULL
    OR flag_deleted <> TRUE -- Cleansing
  GROUP BY
    morders._id,
    morders.created_at,
    morders.level,
    morders.staff_id,
    morders.course_id,
    morders.email
)
-- edumall_user as (
-- SELECT min(_id) as id, email FROM lake.edumall_user group by email
-- ),
-- transactions as (
--   SELECT
--     transactions.siren_id AS siren_id,
--     transactions.price AS transaction_price,
--     transactions.id AS transaction_id,
--     (
--       CASE WHEN transactions.status = 'success' THEN CONVERT_TIMEZONE(
--         'Asia/Ho_Chi_Minh',
--         CONVERT(TIMESTAMP, transactions.received_at)
--       ) END
--     ) AS received_at
--   FROM
--     lake.transactions
--   WHERE transactions.status = 'success'
-- )
SELECT
  morders_stat.*,
  normalize_price(gt.lading_price) AS latest_lading_price,
  to_float(gt.lading_fee) AS latest_lading_fee,
  gt.user_id AS latest_lading_employee_id,
  COALESCE(
    CAST(gt.transaction_price AS REAL),
    CAST(tr.price AS REAL)
    -- CAST(ts.transaction_price AS REAL)
  ) AS latest_transaction_price,
  COALESCE(
    gt.transaction_id,
    tr.id
    -- ts.transaction_id
  ) AS latest_transaction_id,
  gt._id AS latest_lading_id,
  COALESCE(
    gt.received_at,
    tr.received_at
    -- ts.received_at
  ) AS bifrost_l8_updated_at
  -- COALESCE(gt.edumall_payment_id, tr.edumall_payment_id)  as edumall_payment_id
  -- eu.id as user_id
FROM
  morders_stat
  -- LEFT JOIN transactions as ts ON morders_stat._id = ts.siren_id
  LEFT JOIN gorders_ranked_transactions AS gt ON morders_stat._id = gt.siren_id
  AND gt.ranked = 1
  LEFT JOIN transactions_ranked as tr ON tr.siren_id = morders_stat._id
  AND tr.ranked = 1
  AND UPPER(tr.method) <> 'COD'
  -- LEFT JOIN edumall_user as eu ON eu.email = morders_stat.email