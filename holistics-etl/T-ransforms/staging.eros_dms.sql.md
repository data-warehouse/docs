# [staging.eros_dms](../holistics-etl/T-ransforms/staging.eros_dms.sql)

| **Field**                 | **Data Type**            | **Table Lake**                                          | **Description**              | **Note** |
| ------------------------- | ------------------------ | ------------------------------------------------------- | ---------------------------- | -------- |
| _id                       | varchar(1020) (NOT NULL) | lake.morders                                            | id eros_dms                  | 1        |
| created_at                | timestamp (NOT NULL)     | lake.morders                                            | created_at                   |          |
| call_logs_count           | int4                     | lake.morders                                            | số lượng lịch sử cuộc gọi    |          |
| gorders_count             | int4                     | lake.gorders                                            | số lượng đơn vận đơn         |          |
| level                     | varchar(1020)            | lake.morders                                            | level                        |          |
| staff_id                  | varchar(1020)            | lake.morders                                            | staff_id                     |          |
| course_id                 | varchar(1020)            | lake.morders                                            | course_id                    |          |
| l1_staff_id               | varchar(1020)            | lake.history_orders                                     | staff_id tại L1              | 2        |
| l2_staff_id               | varchar(1020)            | lake.history_orders                                     | staff_id tại L2              | 2        |
| l2x_staff_id              | varchar(1020)            | lake.history_orders                                     | staff_id tại L2x             | 2        |
| l3a_staff_id              | varchar(1020)            | lake.history_orders                                     | staff_id tại L3a             | 2        |
| l3b_staff_id              | varchar(1020)            | lake.history_orders                                     | staff_id tại L3b             | 2        |
| l4a_staff_id              | varchar(1020)            | lake.history_orders                                     | staff_id tại L4a             | 2        |
| l4b_staff_id              | varchar(1020)            | lake.history_orders                                     | staff_id tại L4b             | 2        |
| l7_staff_id               | varchar(1020)            | lake.history_orders                                     | staff_id tại L7              | 2        |
| l7a_staff_id              | varchar(1020)            | lake.history_orders                                     | staff_id tại L7a             | 2        |
| l7b_staff_id              | varchar(1020)            | lake.history_orders                                     | staff_id tại L7b             | 2        |
| l8a_staff_id              | varchar(1020)            | lake.history_orders                                     | staff_id tại L8a             | 2        |
| l8b_staff_id              | varchar(1020)            | lake.history_orders                                     | staff_id tại L8b             | 2        |
| l1_at                     | timestamp                | lake.history_orders                                     | thời điểm lên L1             | 2        |
| l2_at                     | timestamp                | lake.history_orders                                     | thời điểm lên L2             | 2        |
| l2x_at                    | timestamp                | lake.history_orders                                     | thời điểm lên L2x            | 2        |
| l3a_at                    | timestamp                | lake.history_orders                                     | thời điểm lên L3a            | 2        |
| l3b_at                    | timestamp                | lake.history_orders                                     | thời điểm lên L3b            | 2        |
| l4a_at                    | timestamp                | lake.history_orders                                     | thời điểm lên L4a            | 2        |
| l4b_at                    | timestamp                | lake.history_orders                                     | thời điểm lên L4b            | 2        |
| l7a_at                    | timestamp                | lake.history_orders                                     | thời điểm lên L7a            | 2        |
| l7b_at                    | timestamp                | lake.history_orders                                     | thời điểm lên L7b            | 2        |
| l8a_at                    | timestamp                | lake.history_orders                                     | thời điểm lên L8a            | 2        |
| l8b_at                    | timestamp                | lake.history_orders                                     | thời điểm lên L8b            | 2        |
| first_l2_at               | timestamp                | lake.history_orders                                     | thời điểm đầu tiên lên L2    | 2        |
| first_l3_at               | timestamp                | lake.history_orders                                     | thời điểm đầu tiên lên L3    | 2        |
| first_l4_at               | timestamp                | lake.history_orders                                     | thời điểm đầu tiên lên L4    | 2        |
| first_l7_at               | timestamp                | lake.history_orders                                     | thời điểm đầu tiên lên L7    | 2        |
| first_l8_at               | timestamp                | lake.history_orders                                     | thời điểm đầu tiên lên L8    | 2        |
| latest_lading_price       | int4                     | lake.gorders<br>lake.gchild_orders<br>lake.transactions | lading_price cuối cùng       | 3, 6     |
| latest_lading_fee         | float4                   | lake.gorders<br>lake.gchild_orders<br>lake.transactions | lading_fee cuối cùng         | 3, 6     |
| latest_lading_employee_id | varchar(1020)            | lake.gorders<br>lake.gchild_orders<br>lake.transactions | lading_employee_id cuối cùng | 3, 6     |
| latest_transaction_price  | float4                   | lake.gorders<br>lake.gchild_orders<br>lake.transactions | transaction_price cuối cùng  | 3, 5, 6  |
| latest_transaction_id     | varchar(1020)            | lake.gorders<br>lake.gchild_orders<br>lake.transactions | transaction_id cuối cùng     | 3, 5, 6  |
| latest_lading_id          | varchar(1020)            | lake.gorders<br>lake.gchild_orders<br>lake.transactions | lading_id cuối cùng          | 3, 6     |
| bifrost_l8_updated_at     | timestamp                | lake.gorders<br>lake.gchild_orders<br>lake.transactions | ngày lên L8 tại bifrost      | 4, 5, 6  |

**Note:**

1. *id eros_dms* được định nghĩa là id bảng *lake.morders*
2. *staff_id/created_at* được định nghĩa là giá trị đầu tiên (theo thứ tự tăng dần created_at) của bảng *history_order* ghi nhận được tại mỗi level
3. giá trị cuối cùng được định nghĩa là giá trị cuối cùng được đánh giá theo thứ tự giảm dần của level vận đơn: *S4 -> S3 -> S2 -> S1*
4. ngày lên L8 tại bifrost được định nghĩa là trường *lake.transactions.received_at* với điều kiện *lake.transactions.status = "success"*
5. khi không tìm thấy bản ghi trên *lake.gorders* (thông tin đơn hàng khác cod) sẽ lấy bản ghi từ thông tin thanh toán, được đánh giá theo thứ tự ưu tiên về trạng thái thanh toán của đơn hàng: *success -> pending -> failed -> canceled*
6. (update 01/03/2018) khi không tìm thấy bản ghi trên *lake.gorders* sẽ lấy bản ghi từ *lake.gchild_orders* là đơn hàng gộp từ nhiều đơn hàng (logic đơn hàng mới trên hệ thống vận đơn psmo)

