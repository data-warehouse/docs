/* staging.mar_dms */

WITH
-- ad_bydates AS (
--   -- (
--     SELECT
--     CAST(ad_bydates.id AS VARCHAR) AS id,
--     ad_bydates.ad_id AS ad_id,
--     ad_bydates.bydate,
--     ad_bydates.c1,
--     ad_bydates.c2,
--     ad_bydates.c3 AS _c3,
--     COUNT(c3copies.id) AS c3,
--     ad_bydates.cost
--   FROM lake.ad_bydates
--   LEFT JOIN lake.ads      ON ads.id = ad_bydates.ad_id
--                           AND ads.deleted_at IS NULL
--   LEFT JOIN lake.c3copies ON MD5(c3copies.utm_medium) = MD5(ads.utm_medium)
--                           AND DATE(c3copies.date) = DATE(ad_bydates.bydate)
--   GROUP BY ad_bydates.id, ad_bydates.ad_id, ad_bydates.bydate, ad_bydates.c1, ad_bydates.c2,
--           ad_bydates.c3, ad_bydates.cost
  -- ) UNION (
  --   SELECT
  --   ('ads_' || CAST(ads.id AS VARCHAR)) AS id,
  --   ads.id AS ad_id,
  --   c3copies.date AS bydate,
  --   NULL AS c1,
  --   NULL AS c2,
  --   NULL AS _c3,
  --   COUNT(c3copies.id) AS c3,
  --   NULL AS cost
  -- FROM lake.ads
  -- LEFT JOIN lake.c3copies ON c3copies.utm_medium = ads.utm_medium
  --                         AND ads.deleted_at IS NULL
  -- WHERE NOT EXISTS(SELECT NULL FROM lake.ad_bydates WHERE ad_bydates.ad_id = ads.id)
  -- GROUP BY ads.id, c3copies.id, c3copies.date
  -- )
-- ),
contactc3_staging AS (
  SELECT
    contactc3s._id,
    contactc3s.course_id,
    contactc3s.source_url,
    CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, contactc3s.created_at)) AS created_at,
    DATE(CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, contactc3s.created_at))) AS date_created_at,
    normalize_price(landingpage.old_price) AS old_price,
    normalize_price(landingpage.new_price) AS new_price
  FROM lake.contactc3s
  LEFT JOIN lake.landingpage ON extract_hostname(contactc3s.source_url) = (landingpage.domain || '.' || landingpage.pedia_domain)
),
c3copy_staging AS (
  SELECT
    c3copies.id,
    c3copies.date,
    c3copies.course_id,
    MD5(c3copies.utm_medium) AS md5_utm_medium
  FROM lake.c3copies
),
ad_bydate_staging AS (
  SELECT
    ad_bydates.id,
    ad_bydates.cost,
    ad_bydates.ad_id,
    ad_bydates.bydate
  FROM lake.ad_bydates
)
SELECT
    cts._id                                                                        AS _id,
    cts.created_at                                                                 AS created_at,
    cts.date_created_at                                                            AS bydate,
    cts.old_price                                                                  AS price,
    cts.new_price                                                                  AS promotion_price,
    ads.id                                                                         AS ad_id,
    COALESCE(CAST(abd.id AS VARCHAR), ('ads_' || CAST(ads.id AS VARCHAR) ||
    '_users_' || ISNULL(CAST(marketers.id AS VARCHAR), 'unknown') ||
    '_grusers_' || ISNULL(CAST(mar_teams_marketers.id AS VARCHAR), 'unknown')))    AS ad_bydate_id,
    (abd.cost/NULLIF(COUNT(c3s.id), 0))                                            AS cost,
    ISNULL(course_rates.total_rate, 7.7e-01)                                       AS total_rate,
    cts.course_id                                                                  AS course_id,
    cts.source_url                                                                 AS source_url
FROM contactc3_staging AS cts
LEFT JOIN c3copy_staging AS c3s       ON cts._id = c3s.id
LEFT JOIN lake.ads                    ON MD5(ads.utm_medium) = c3s.md5_utm_medium
                                      AND ads.deleted_at IS NULL
LEFT JOIN ad_bydate_staging AS abd    ON abd.ad_id = ads.id
                                      AND DATE(c3s.date) = DATE(abd.bydate)
LEFT JOIN lake.mar_courses            ON mar_courses.source_id = c3s.course_id
                                      AND mar_courses.deleted_at IS NULL
LEFT JOIN lake.course_rates           ON course_rates.course_id = mar_courses.id
                                      AND course_rates.deleted_at IS NULL
                                      AND cts.date_created_at >= DATE(course_rates.start_date)
                                      AND cts.date_created_at < DATE(course_rates.end_date)
LEFT JOIN lake.ad_groups              ON ad_groups.id = ads.ad_group_id
                                      AND ad_groups.deleted_at IS NULL
LEFT JOIN lake.campaigns              ON campaigns.id = ad_groups.campaign_id
                                      AND campaigns.deleted_at IS NULL
LEFT JOIN lake.channel_course_users   ON channel_course_users.channel_course_id = campaigns.channel_course_id
                                      AND cts.date_created_at >= DATE(channel_course_users.start_date)
                                      AND cts.date_created_at < DATE(channel_course_users.end_date)
                                      AND channel_course_users.deleted_at IS NULL
LEFT JOIN lake.marketers              ON marketers.id = channel_course_users.user_id
                                      AND marketers.deleted_at IS NULL
LEFT JOIN lake.mar_teams_marketers    ON mar_teams_marketers.user_id = marketers.id
                                      AND cts.date_created_at >= DATE(mar_teams_marketers.start_date)
                                      AND cts.date_created_at < DATE(mar_teams_marketers.end_date)
                                      AND mar_teams_marketers.deleted_at IS NULL
GROUP BY  cts._id, cts.created_at, cts.date_created_at, cts.course_id, cts.source_url, cts.old_price, cts.new_price,
          abd.id, abd.ad_id, abd.bydate, abd.cost, ads.id, marketers.id, mar_teams_marketers.id, course_rates.total_rate