# [staging.mar_dms](../holistics-etl/T-ransforms/staging.mar_dms.sql)

| **Field**       | **Data Type**            | **Table Lake**                               | **Description**            | **Note** |
| --------------- | ------------------------ | -------------------------------------------- | -------------------------- | -------- |
| id              | varchar(1020) (NOT NULL) | lake.contactc3s                              | id mar_dms                 | 1        |
| created_at      | timestamp (NOT NULL)     | lake.contactc3s                              | thời điểm tạo contactc3    | 2        |
| bydate          | date                     | lake.contactc3s                              | gói (ngày tạo) contactc3   | 2        |
| price           | int4                     | lake.landingpage                             | giá bán                    |          |
| promotion_price | int4                     | lake.landingpage                             | giá thực thu               |          |
| ad_id           | int4                     | lake.ads                                     | ad_id                      | 4        |
| ad_bydate_id    | varchar(255)             | lake.ad_bydates<br>lake.ads<br>lake.c3copies | ad_bydate_id               | 4        |
| cost            | float4                   | lake.ad_bydates<br>lake.ads<br>lake.c3copies | chi phí quảng cáo          | 4        |
| total_rate      | varchar(255)             | lake.course_rates<br>lake.mar_courses        | tỉ lệ chuyển đổi doanh thu | 3, 4     |
| course_id       | varchar(255)             | lake.contactc3s<br>lake.mar_courses          | course_id                  |          |
| source_url      | varchar(8332)            | lake.contactc3s                              | source_url                 |          |

**Note:**

1. *id mar_dms* được định nghĩa là id bảng *lake.contactc3s*. Khi không tìm thấy id từ bảng đó sẽ tạo id dựa theo thông tin quảng cáo với cấu trúc:   *'ads_' + ads.id*
2. timezone : *Asia/Ho_Chi_Minh*
3. mặc định *total_rate = 0.77*, giá trị thay đổi phụ thuộc vào thời điểm tạo contactc3: *course_rates.start_date >= contactc3s.created_at >= course_rates.end_date*
4. các bản ghi lấy từ database nami cần loại bỏ các bản ghi soft delete: **.deleted_at IS NULL*

