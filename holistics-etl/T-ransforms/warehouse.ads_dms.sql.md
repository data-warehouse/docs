# [warehouse.ads_dms](../holistics-etl/T-ransforms/warehouse.ads_dms.sql)

| **Field**        | **Data Type**            | **Table Lake**                                               | **Description**                         | **Note** |
| ---------------- | ------------------------ | ------------------------------------------------------------ | --------------------------------------- | -------- |
| id               | varchar(1020) (NOT NULL) | lake.ads<br>lake.ad_bydates                                  | id ads_dms                              | 1        |
| ad_name          | varchar(1020)            | lake.ads<br>lake.ad_bydates                                  | tên mẫu quảng cáo                       |          |
| ad_link_tracking | varchar(8332)            | lake.ads<br>lake.ad_bydates                                  | link tracking quảng cáo                 |          |
| ad_account       | varchar(1020)            | lake.adaccounts<br>lake.campaigns                            | tài khoản quảng cáo                     |          |
| ad_channel       | varchar(40)              | lake.channels<br>lake.channel_courses                        | kênh quảng cáo                          |          |
| ad_created_by    | varchar(1020)            | lake.marketers<br>lake.channel_course_users<br>lake.mar_teams | người tạo                               |          |
| ad_group         | varchar(1020)            | lake.ad_groups<br>lake.campaigns<br>lake.ads                 | ad group                                |          |
| ad_team_lead     | varchar(1020)            | lake.marketers<br>lake.mar_departments                       | team lead marketer thời điểm ra contact |          |
| department       | varchar(1020)            | lake.mar_departments                                         | bộ phận marketing                       | 2        |
| group_name       | varchar(1020)            | lake.mar_departments                                         | tên nhóm marketing                      | 3        |

**Note:**

1. id *ads_dms* được định nghĩa là id bảng *lake.ad_bydates*. Khi không tìm thấy id từ bảng đó sẽ tạo id dựa theo thông tin quảng cáo với cấu trúc:   *'ads_' + ads.id*.
2. *department* được định nghĩa tại bảng *lake.mar_departments*, là những bản ghi có điều kiện là *lake.mar_departments.type = 'Division'*
3. *group_name* được định nghĩa tại bảng *lake.mar_departments*, là những bản ghi có điều kiện là *lake.mar_departments.type = 'Group'* và *lake.mar_departments.id = lake.mar_departments.sup_department_id*