/*TrungND update 30/8 */
/* warehouse.course_dms */
WITH
category_parent AS (
    SELECT categories._id, categories.name
    FROM lake.categories
    WHERE categories.parent_category_id IS NULL
),
pry_category AS (
    SELECT primary_categories._id, primary_categories.name,primary_categories.course_id0,
  json_array_length(primary_categories.course_id0) as total_course
    FROM lake.primary_categories
)
(SELECT
-- Single courses
    courses._id AS c_course_id, /* PRIMARY KEY */
    courses.code AS c_course_code,
    courses.name AS c_course_name,
    users.username AS c_teacher_name,
    users.email AS c_teacher_email,
    courses.instructor_code AS c_teacher_code,
    NULL AS c_course_price,
    CAST(courses.price AS INTEGER) AS c_course_real_price,
    -- CANCELLED AS c_start_date,
    -- CANCELLED AS c_end_date,
    (SELECT category_parent.name FROM category_parent WHERE CHARINDEX(category_parent._id, courses.category_id0) > 0 LIMIT 1) AS c_category,
    (SELECT category_parent._id FROM category_parent WHERE CHARINDEX(category_parent._id, courses.category_id0) > 0 LIMIT 1) AS c_category_id,
    (SELECT pry_category.name FROM pry_category WHERE CHARINDEX(courses._id, pry_category.course_id0) > 0 LIMIT 1) AS c_pry_category,
    (SELECT pry_category._id FROM pry_category WHERE CHARINDEX(courses._id, pry_category.course_id0) > 0 LIMIT 1) AS c_pry_category_id,
    'course',
    --COALESCE(c_pry_category, c_category) AS c_pry_category_id,

    mar_courses.category_name AS course_group
FROM lake.courses
LEFT JOIN lake.users ON users._id = courses.user_id
LEFT JOIN lake.mar_courses ON mar_courses.source_id = courses._id
) UNION ALL (
-- Combo courses
WITH
courses_seq AS(
    -- SELECT
    --     ROW_NUMBER() OVER (ORDER BY TRUE)::INTEGER- 1 as i
    -- FROM lake.courses
    -- LIMIT 10
    -- This number specify the max number of courses included in ONE combo
    SELECT 0 AS i UNION
    SELECT 1 AS i UNION
    SELECT 2 AS i UNION
    SELECT 3 AS i UNION
    SELECT 4 AS i UNION
    SELECT 5 AS i UNION
    SELECT 6 AS i UNION
    SELECT 7 AS i UNION
    SELECT 8 AS i UNION
    SELECT 9 AS i
),
exploded_sale_packages_ranked AS (
    SELECT
        sale_packages.*,
        JSON_EXTRACT_PATH_TEXT(
            JSON_EXTRACT_ARRAY_ELEMENT_TEXT(sale_packages.course_ids, seq.i),
            '$oid'
        ) AS course_id,
        ROW_NUMBER() OVER (PARTITION BY sale_packages.code ORDER BY sale_packages.created_at) AS ranked,
        ROW_NUMBER() OVER (PARTITION BY c_pry_category ORDER BY c_pry_total_course) AS ranked_category,

        (SELECT pry_category.name FROM pry_category WHERE CHARINDEX(course_id, pry_category.course_id0) > 0 LIMIT 1) AS c_pry_category,
        (SELECT pry_category.total_course FROM pry_category WHERE CHARINDEX(course_id, pry_category.course_id0) > 0 LIMIT 1) AS c_pry_total_course

    FROM lake.sale_packages, courses_seq AS seq, pry_category
    WHERE seq.i < JSON_ARRAY_LENGTH(sale_packages.course_ids)
)
SELECT
    DISTINCT(espr.code)                                             AS c_course_id, /* PRIMARY KEY */
    MAX(espr.code)                                                  AS c_course_code,
    MAX(espr.title)                                                 AS c_course_name,
    f_distinct_listagg(LISTAGG(users.username, ','))                AS c_teacher_name,
    f_distinct_listagg(LISTAGG(users.email   , ','))                AS c_teacher_email,
    f_distinct_listagg(LISTAGG(courses.instructor_code   , ','))    AS c_teacher_code,
    NULL                                                            AS c_course_price,
    MAX(CAST(espr.price AS INTEGER))                                AS c_course_real_price,
    -- CANCELLED AS c_start_date,
    -- CANCELLED AS c_end_date,
    NULL                                                            AS c_category,
    NULL                                                            AS c_category_id,
    MAX(espr.c_pry_category)                                        AS c_pry_category,
    NULL                                                            AS c_pry_category_id,
    'combo',
    f_distinct_listagg(LISTAGG(mar_courses.category_name , ','))    AS course_group
FROM exploded_sale_packages_ranked AS espr
LEFT JOIN lake.courses ON courses._id = espr.course_id
LEFT JOIN lake.users ON users._id = courses.user_id
LEFT JOIN lake.mar_courses ON mar_courses.source_id = espr.course_id
WHERE espr.code IS NOT NULL AND espr.ranked = 1
GROUP BY espr._id, espr.code
)