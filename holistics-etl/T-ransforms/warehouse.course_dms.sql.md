# [warehouse.course_dms](../holistics-etl/T-ransforms/warehouse.course_dms.sql)

| **Field**           | **Data Type**  | **Table Lake**                     | **Description**         | **Note** |
| ------------------- | -------------- | ---------------------------------- | ----------------------- | -------- |
| c_course_id         | varchar(1020)  | lake.courses<br>lake.sale_packages | id course_dms           | 1, 3     |
| c_course_code       | varchar(1020)  | lake.courses<br>lake.sale_packages | mã khoá học             | 3        |
| c_course_name       | varchar(1020)  | lake.courses<br>lake.sale_packages | tên khoá học            | 3        |
| c_teacher_name      | varchar(65535) | lake.users                         | tên giảng viên          |          |
| c_teacher_email     | varchar(65535) | lake.users                         | email giảng viên        |          |
| c_teacher_code      | varchar(65535) | lake.courses                       | mã thầy                 |          |
| c_course_price      | varchar(65535) | no table                           | giá bán khoá học        | 2        |
| c_course_real_price | int4           | lake.courses<br>lake.sale_packages | giá niêm yết khoá học   | 3        |
| c_category          | varchar(65535) | lake.categories                    | category                |          |
| c_category_id       | varchar(65535) | lake.categories                    | mã category             |          |
| course_group        | varchar(1020)  | lake.mar_courses                   | nhóm khóa học marketing |          |

**Note:**

1. *id course_dms* được định nghĩa là id của *lake.courses* hoặc trường code của *lake.sale_packages*
2. *c_course_price* được định nghĩa ở trường *old_price* bảng *lake.courses* (trường này có rất ít giá trị nên không đưa vào do giá bán không được định nghĩa ở bảng *lake.courses*)
3. khóa học (*course*) được định nghĩa bao gồm khóa học (bảng *lake.courses*) và combo khóa học (bảng *lake.sale_packages*) gồm nhiều khóa học (tối đa 9 khóa học)