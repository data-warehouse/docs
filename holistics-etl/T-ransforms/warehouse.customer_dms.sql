/* warehouse.customer_dms */

(SELECT
    COALESCE(mo.name, contactc3s.name) AS cd_name,
    COALESCE(mo.mobile, contactc3s.mobile) AS cd_mobile,
    COALESCE(mo.email, contactc3s.email) AS cd_email,
    --PENDING: AS cd_mobile2,
    --PENDING: AS cd_email2,
    --PENDING: AS cd_gender,
    mo.p_province AS cd_province,
    mo.district AS cd_district,
    mo.address AS cd_address,
    --PENDING: AS cd_address2,
    mo.p_nation AS cd_nation,
    mo.p_region AS cd_region,
    COALESCE(mo._id, contactc3s._id) AS cd_id /* PRIMARY KEY */
FROM lake.contactc3s
FULL OUTER JOIN (
    SELECT
        morders.*,
        provinces.name   AS p_province,
        provinces.region AS p_region,
        provinces.nation AS p_nation
    FROM lake.morders
    LEFT JOIN lake.provinces ON provinces.code = morders.province
    WHERE morders.flag_deleted IS NULL OR morders.flag_deleted <> TRUE  -- Cleansing
) AS mo ON contactc3s._id = mo._id
) UNION ALL (
-- Direct (Go straight to system without advertising (nami) or sale convert (eros))
SELECT
    bbuyers.name AS cd_name,
    bbuyers.mobile AS cd_mobile,
    bbuyers.email AS cd_email,
    --PENDING: AS cd_mobile2,
    --PENDING: AS cd_email2,
    --PENDING: AS cd_gender,
    NULL AS cd_province,
    NULL AS cd_district,
    bbuyers.address AS cd_address,
    --PENDING: AS cd_address2,
    NULL AS cd_nation,
    NULL AS cd_region,
    tr.transaction_course_id AS cd_id /* PRIMARY KEY */
FROM staging.bifrost_dms AS tr
LEFT JOIN lake.bbuyers ON bbuyers.id = tr.buyer_id
WHERE tr.ranked = 1
GROUP BY bbuyers.mobile, bbuyers.email, bbuyers.name, bbuyers.address
)