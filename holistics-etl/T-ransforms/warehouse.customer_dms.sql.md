# [warehouse.customer_dms](../holistics-etl/T-ransforms/warehouse.customer_dms.sql)

| **Field**   | **Data Type**            | **Table Lake**                                               | **Description**           | **Note** |
| ----------- | ------------------------ | ------------------------------------------------------------ | ------------------------- | -------- |
| cd_name     | varchar(8332)            | lake.morders<br>lake.contactc3s<br>lake.bbuyers              | tên khách hàng            | 1, 3, 5  |
| cd_mobile   | varchar(8332)            | lake.morders<br>lake.contactc3s<br>lake.bbuyers              | số điện thoại khách hàng  | 1, 3, 5  |
| cd_email    | varchar(8332)            | lake.morders<br>lake.contactc3s<br>lake.bbuyers              | email khách hàng          | 1, 3, 5  |
| cd_province | varchar(255)             | lake.provinces<br>lake.morders                               | tỉnh                      | 2, 3     |
| cd_district | varchar(1020)            | lake.provinces<br>lake.morders                               | huyện                     | 2, 3     |
| cd_address  | varchar(6000)            | lake.provinces<br>lake.morders                               | địa chỉ khách hàng cụ thể | 2, 3     |
| cd_nation   | varchar(255)             | lake.provinces<br>lake.morders                               | quốc gia                  | 2, 3     |
| cd_region   | varchar(255)             | lake.provinces<br>lake.morders                               | vùng miền                 | 2, 3     |
| cd_id       | varchar(1020) (NOT NULL) | lake.morders<br>lake.contactc3s<br>lake.transactions<br>staging.bifrost_dms | id customer_dms           | 1, 3, 4  |

**Note:**

1. Thông tin khách hàng được định nghĩa là thông tin lấy từ đơn hàng hoặc contact c3 hoặc thông tin thanh toán

2. địa chỉ khách hàng đăng ký khóa học

3. bảng *lake.morders* không bao gồm bản ghi của những đơn hàng bị xóa: *morders.flag_deleted <> TRUE*

4. *id customer_dms* được định nghĩa là id của bảng *lake.morders* hoặc *lake.contactc3s* (id 2 bảng này trùng nhau). Khi không tìm thấy khách hàng trong 2 bảng này thì lấy transaction_course_id của *staging.bifrost_dms* với cấu trúc: *'dir_' + lake.transactions.buyer_id + lake.bcourses.id*

5. khi không tìm được thông tin trên contact c3 hoặc đơn hàng sẽ lấy thông tin trên *staging.bifrost_dms*
