/* warehouse.date_dms */

(
-- C3 dim
SELECT
    COALESCE(mar_dms._id, eros_dms._id)                                             AS id,
    DATE(DATEADD(HOUR, 2, COALESCE(mar_dms.created_at, eros_dms.created_at)))       AS d_block,
    DATE_PART(HOUR,  COALESCE(mar_dms.created_at, eros_dms.created_at))             AS d_hour,
    DATE_PART(MIN,   COALESCE(mar_dms.created_at, eros_dms.created_at))             AS d_minute,
    CONVERT(TIMESTAMP, COALESCE(mar_dms.created_at, eros_dms.created_at))           AS d_day,
    DATE_PART(WEEK,  DATE(COALESCE(mar_dms.created_at, eros_dms.created_at)))       AS d_week,
    DATE_PART(MONTH, DATE(COALESCE(mar_dms.created_at, eros_dms.created_at)))       AS d_month,
    DATE_PART(YEAR,  DATE(COALESCE(mar_dms.created_at, eros_dms.created_at)))       AS d_year,
    DATE_PART(QTR,   DATE(COALESCE(mar_dms.created_at, eros_dms.created_at)))       AS d_quater,
    (CASE DATE_PART(DW, DATE(COALESCE(mar_dms.created_at, eros_dms.created_at)))
        WHEN 0 THEN 'Chủ nhật'
        WHEN 1 THEN 'Thứ 2'
        WHEN 2 THEN 'Thứ 3'
        WHEN 3 THEN 'Thứ 4'
        WHEN 4 THEN 'Thứ 5'
        WHEN 5 THEN 'Thứ 6'
        WHEN 6 THEN 'Thứ 7'
    END)                                                                            AS d_day_of_week,
    DATE_PART(DAY,   DATE(COALESCE(mar_dms.created_at, eros_dms.created_at)))       AS d_day_of_month,
    DATE_PART(DOY,   DATE(COALESCE(mar_dms.created_at, eros_dms.created_at)))       AS d_day_of_year
FROM staging.mar_dms
FULL OUTER JOIN staging.eros_dms
ON eros_dms._id = mar_dms._id
) UNION ALL (
-- C2, C1 dim
SELECT
    CAST(ad_bydates.id AS VARCHAR)                  AS id,
    NULL                                            AS d_block,
    NULL                                            AS d_hour,
    NULL                                            AS d_minute,
    CONVERT(TIMESTAMP, ad_bydates.bydate)           AS d_day,
    DATE_PART(WEEK,  DATE(ad_bydates.bydate))       AS d_week,
    DATE_PART(MONTH, DATE(ad_bydates.bydate))       AS d_month,
    DATE_PART(YEAR,  DATE(ad_bydates.bydate))       AS d_year,
    DATE_PART(QTR,   DATE(ad_bydates.bydate))       AS d_quater,
    (CASE DATE_PART(DW, DATE(ad_bydates.bydate))
        WHEN 0 THEN 'Chủ nhật'
        WHEN 1 THEN 'Thứ 2'
        WHEN 2 THEN 'Thứ 3'
        WHEN 3 THEN 'Thứ 4'
        WHEN 4 THEN 'Thứ 5'
        WHEN 5 THEN 'Thứ 6'
        WHEN 6 THEN 'Thứ 7'
    END)                                            AS d_day_of_week,
    DATE_PART(DAY,   DATE(ad_bydates.bydate))       AS d_day_of_month,
    DATE_PART(DOY,   DATE(ad_bydates.bydate))       AS d_day_of_year
FROM lake.ad_bydates
WHERE c3 IS NULL OR c3 = 0
) UNION ALL (
-- Direct (Go straight to system without advertising (nami) or sale convert (eros))
SELECT
    tr.transaction_course_id AS id,
    DATE(DATEADD(HOUR, 2,    MIN(tr.created_at))) AS d_block,
    DATE_PART(HOUR,          MIN(tr.created_at))  AS d_hour,
    DATE_PART(MIN,           MIN(tr.created_at))  AS d_minute,
    CONVERT(TIMESTAMP,       MIN(tr.created_at))  AS d_day,
    DATE_PART(WEEK,     DATE(MIN(tr.created_at))) AS d_week,
    DATE_PART(MONTH,    DATE(MIN(tr.created_at))) AS d_month,
    DATE_PART(YEAR,     DATE(MIN(tr.created_at))) AS d_year,
    DATE_PART(QTR,      DATE(MIN(tr.created_at))) AS d_quater,
    (CASE DATE_PART(DW, DATE(MIN(tr.created_at)))
        WHEN 0 THEN 'Chủ nhật'
        WHEN 1 THEN 'Thứ 2'
        WHEN 2 THEN 'Thứ 3'
        WHEN 3 THEN 'Thứ 4'
        WHEN 4 THEN 'Thứ 5'
        WHEN 5 THEN 'Thứ 6'
        WHEN 6 THEN 'Thứ 7'
    END) AS d_day_of_week,
    DATE_PART(DAY,      DATE(MIN(tr.created_at))) AS d_day_of_month,
    DATE_PART(DOY,      DATE(MIN(tr.created_at))) AS d_day_of_year
FROM staging.bifrost_dms AS tr
WHERE tr.ranked = 1
GROUP BY tr.transaction_course_id
)
