# [ warehouse.date_dms](../holistics-etl/T-ransforms/warehouse.date_dms.sql)

| **Field**      | **Data Type**            | **Table Lake**                                               | **Description**                              | **Note** |
| -------------- | ------------------------ | ------------------------------------------------------------ | -------------------------------------------- | -------- |
| id             | varchar(1020) (NOT NULL) | staging.mar_dms<br>staging.eros_dms<br>lake.transactions<br>lake.bcourses<br>staging.bifrost_dms | id date_dms                                  | 1        |
| d_block        | date                     | staging.mar_dms<br>staging.eros_dms<br>lake.transactions<br>staging.bifrost_dms | thời điểm đăng ký thuộc ngày nào trong năm   | 3, 4     |
| d_hour         | int4                     | staging.mar_dms<br>staging.eros_dms<br>lake.transactions<br>staging.bifrost_dms | thời điểm đăng ký thuộc giờ nào trong ngày   | 3, 4     |
| d_minute       | int4                     | staging.mar_dms<br>staging.eros_dms<br>lake.transactions<br>staging.bifrost_dms | thời điểm đăng ký thuộc phút nào trong giờ   | 3, 4     |
| d_day          | timestamp                | staging.mar_dms<br>staging.eros_dms<br>lake.transactions<br>staging.bifrost_dms | thời điểm đăng ký contact                    | 2, 3, 4  |
| d_week         | int4                     | staging.mar_dms<br>staging.eros_dms<br>lake.transactions<br>staging.bifrost_dms | thời điểm đăng ký thuộc tuần nào trong năm   | 3, 4     |
| d_month        | int4                     | staging.mar_dms<br>staging.eros_dms<br>lake.transactions<br>staging.bifrost_dms | thời điểm đăng ký thuộc tháng nào trong năm  | 3, 4     |
| d_year         | int4                     | staging.mar_dms<br>staging.eros_dms<br>lake.transactions<br>staging.bifrost_dms | thời điểm đăng ký là năm nào                 | 3, 4     |
| d_quater       | int4                     | staging.mar_dms<br>staging.eros_dms<br>lake.transactions<br>staging.bifrost_dms | thời điểm đăng ký thuộc quý nào trong năm    | 3, 4     |
| d_day_of_week  | varchar(12)              | staging.mar_dms<br>staging.eros_dms<br>lake.transactions<br>staging.bifrost_dms | thời điểm đăng ký thuộc thứ mấy trong tuần   | 3, 4     |
| d_day_of_month | int4                     | staging.mar_dms<br>staging.eros_dms<br>lake.transactions<br>staging.bifrost_dms | thời điểm đăng ký thuộc ngày nào trong tháng | 3, 4     |
| d_day_of_year  | int4                     | staging.mar_dms<br>staging.eros_dms<br>lake.transactions<br>staging.bifrost_dms | thời điểm đăng ký thuộc ngày nào trong năm   | 3, 4     |

**Note:**

1. *id date_dms* được định nghĩa là id của bảng *stagging.mar_dms* hoặc *stagging.eros_dms* hoặc *lake.ad_bydates*. Khi không tìm thấy id từ 2 bảng đó sẽ lấy transaction_course_id của *staging.bifrost_dms* với cấu trúc:   *'dir_' + lake.transactions.buyer_id + lake.bcourses.id*
2. thời điểm đăng ký contact là của contact c3 *(stagging.mar_dms, stagging.eros_dms)* hoặc contact c2, c1 *(lake.ad_bydates)* nếu không có contact c3
3. timezone : *Asia/Ho_Chi_Minh*
4. ​khi không tìm được thông tin trên contact c3 hoặc đơn hàng sẽ lấy thông tin trên *staging.bifrost_dms*