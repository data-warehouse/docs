/* warehouse.lading_date_dms */

WITH
gorders_ranked AS (
    SELECT
        gorders._id,
        gorders.siren_id,
        CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, gorders.created_at))        AS created_at,
        CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, gorders.ship_at))           AS ship_at,
        CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, gorders.created_at))        AS order_at,
        CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, gorders.received_at))       AS received_at,
        CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, gorders.received_estimate)) AS estimate_at,
        ROW_NUMBER() OVER (PARTITION BY gorders.siren_id ORDER BY gorders.created_at DESC)  AS ranked,
        MAX(CASE WHEN gpartner_statuses.s_level_child = 'S2'   THEN CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, gorder_trackings.partner_time)) END) AS tracking_order_s2_at,
        MAX(CASE WHEN gpartner_statuses.s_level_child = 'S3A'  THEN CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, gorder_trackings.partner_time)) END) AS tracking_order_s3a_at,
        MAX(CASE WHEN gpartner_statuses.s_level_child = 'S3B'  THEN CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, gorder_trackings.partner_time)) END) AS tracking_order_s3b_at,
        MAX(CASE WHEN gpartner_statuses.s_level_child = 'S3C'  THEN CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, gorder_trackings.partner_time)) END) AS tracking_order_s3c_at,
        MAX(CASE WHEN gpartner_statuses.s_level_child = 'S3D'  THEN CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, gorder_trackings.partner_time)) END) AS tracking_order_s3d_at,
        MAX(CASE WHEN gpartner_statuses.s_level_child = 'S3TC' THEN CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, gorder_trackings.partner_time)) END) AS tracking_order_s3tc_at,
        MAX(CASE WHEN gpartner_statuses.s_level_child = 'S3.3' THEN CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, gorder_trackings.partner_time)) END) AS tracking_order_s33_at,
        MAX(CASE WHEN gpartner_statuses.s_level_child = 'S4'   THEN CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, gorder_trackings.partner_time)) END) AS tracking_order_s4_at
    FROM lake.gorders
    LEFT JOIN lake.gorder_trackings ON gorder_trackings.order_id = gorders._id
    LEFT JOIN lake.gpartner_statuses ON gorder_trackings.partner_status_id = gpartner_statuses._id
    GROUP BY gorders._id, gorders.siren_id, gorders.created_at, gorders.ship_at, gorders.received_at, gorders.received_estimate
),
-- glogs_order_s2 AS (
--     SELECT
--         logable_id                                                                  AS order_id,
--         CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, created_at))        AS created_at,
--         ROW_NUMBER() OVER (PARTITION BY logable_id ORDER BY created_at ASC)         AS ranked
--     FROM lake.glogs
--     WHERE logable_type = 'Order'
--     AND type = 'update'
--     AND s_level1 IN ('S2','S2I')
-- ),
m_order AS (
    SELECT
        morders._id
    FROM lake.morders
    WHERE morders.flag_deleted IS NULL OR flag_deleted <> TRUE
)
(SELECT
    DISTINCT m_order._id                                                        AS id,
    -- Chorus: ship_at
    order_date_helper(gorders_ranked.ship_at, 'hour')                           AS d_ship_at_hour,
    order_date_helper(gorders_ranked.ship_at, 'block')                          AS d_ship_at_block,
    DATE(gorders_ranked.ship_at)                                                AS d_ship_at_day,
    DATE_PART(WEEK,   gorders_ranked.ship_at)                                   AS d_ship_at_week,
    DATE_PART(MONTH,  gorders_ranked.ship_at)                                   AS d_ship_at_month,
    DATE_PART(YEAR,  gorders_ranked.ship_at)                                    AS d_ship_at_year,
    DATE_PART(QTR,    gorders_ranked.ship_at)                                   AS d_ship_at_quater,
    DATE_PART(DW,     gorders_ranked.ship_at)                                   AS d_ship_at_day_of_week,
    DATE_PART(DAY,    gorders_ranked.ship_at)                                   AS d_ship_at_day_of_month,
    DATE_PART(DOY,    gorders_ranked.ship_at)                                   AS d_ship_at_day_of_year,
    gorders_ranked.ship_at                                                      AS d_ship_at_fulltime,
    DATEDIFF(DAY, gorders_ranked.created_at, gorders_ranked.ship_at)            AS d_ship_at_date_t_since_order_at,
    -- Chorus: order_at
    order_date_helper(gorders_ranked.order_at, 'hour')                          AS d_order_at_hour,
    order_date_helper(gorders_ranked.order_at, 'block')                         AS d_order_at_block,
    DATE(gorders_ranked.order_at)                                               AS d_order_at_day,
    DATE_PART(WEEK,   gorders_ranked.order_at)                                  AS d_order_at_week,
    DATE_PART(MONTH,  gorders_ranked.order_at)                                  AS d_order_at_month,
    DATE_PART(YEAR,   gorders_ranked.order_at)                                  AS d_order_at_year,
    DATE_PART(QTR,    gorders_ranked.order_at)                                  AS d_order_at_quater,
    DATE_PART(DW,     gorders_ranked.order_at)                                  AS d_order_at_day_of_week,
    DATE_PART(DAY,    gorders_ranked.order_at)                                  AS d_order_at_day_of_month,
    DATE_PART(DOY,    gorders_ranked.order_at)                                  AS d_order_at_day_of_year,
    gorders_ranked.order_at                                                     AS d_order_at_fulltime,
    DATEDIFF(DAY, gorders_ranked.created_at, gorders_ranked.order_at)           AS d_order_at_date_t_since_order_at,
    -- Chorus: recied_at
    order_date_helper(gorders_ranked.received_at, 'hour')                       AS d_received_at_hour,
    order_date_helper(gorders_ranked.received_at, 'block')                      AS d_received_at_block,
    DATE(gorders_ranked.received_at)                                            AS d_received_at_day,
    DATE_PART(WEEK,   gorders_ranked.received_at)                               AS d_received_at_week,
    DATE_PART(MONTH,  gorders_ranked.received_at)                               AS d_received_at_month,
    DATE_PART(YEAR,   gorders_ranked.received_at)                               AS d_received_at_year,
    DATE_PART(QTR,    gorders_ranked.received_at)                               AS d_received_at_quater,
    DATE_PART(DW,     gorders_ranked.received_at)                               AS d_received_at_day_of_week,
    DATE_PART(DAY,    gorders_ranked.received_at)                               AS d_received_at_day_of_month,
    DATE_PART(DOY,    gorders_ranked.received_at)                               AS d_received_at_day_of_year,
    gorders_ranked.received_at                                                  AS d_received_at_fulltime,
    DATEDIFF(DAY, gorders_ranked.created_at, gorders_ranked.received_at)        AS d_received_at_date_t_since_order_at,
    -- Chorus: estimate_at
    order_date_helper(gorders_ranked.estimate_at, 'hour')                       AS d_estimate_at_hour,
    order_date_helper(gorders_ranked.estimate_at, 'block')                      AS d_estimate_at_block,
    DATE(gorders_ranked.estimate_at)                                            AS d_estimate_at_day,
    DATE_PART(WEEK,   gorders_ranked.estimate_at)                               AS d_estimate_at_week,
    DATE_PART(MONTH,  gorders_ranked.estimate_at)                               AS d_estimate_at_month,
    DATE_PART(YEAR,   gorders_ranked.estimate_at)                               AS d_estimate_at_year,
    DATE_PART(QTR,    gorders_ranked.estimate_at)                               AS d_estimate_at_quater,
    DATE_PART(DW,     gorders_ranked.estimate_at)                               AS d_estimate_at_day_of_week,
    DATE_PART(DAY,    gorders_ranked.estimate_at)                               AS d_estimate_at_day_of_month,
    DATE_PART(DOY,    gorders_ranked.estimate_at)                               AS d_estimate_at_day_of_year,
    gorders_ranked.estimate_at                                                  AS d_estimate_at_fulltime,
    DATEDIFF(DAY, gorders_ranked.created_at, gorders_ranked.estimate_at)        AS d_estimate_at_date_t_since_order_at,
    -- Chorus: printed_at
    -- order_date_helper(glogs_order_s2.created_at, 'hour')                        AS d_printed_at_hour,
    -- order_date_helper(glogs_order_s2.created_at, 'block')                       AS d_printed_at_block,
    -- DATE(glogs_order_s2.created_at)                                             AS d_printed_at_day,
    -- DATE_PART(WEEK,   glogs_order_s2.created_at)                                AS d_printed_at_week,
    -- DATE_PART(MONTH,  glogs_order_s2.created_at)                                AS d_printed_at_month,
    -- DATE_PART(YEAR,   glogs_order_s2.created_at)                                AS d_printed_at_year,
    -- DATE_PART(QTR,    glogs_order_s2.created_at)                                AS d_printed_at_quater,
    -- DATE_PART(DW,     glogs_order_s2.created_at)                                AS d_printed_at_day_of_week,
    -- DATE_PART(DAY,    glogs_order_s2.created_at)                                AS d_printed_at_day_of_month,
    -- DATE_PART(DOY,    glogs_order_s2.created_at)                                AS d_printed_at_day_of_year,
    -- glogs_order_s2.created_at                                                   AS d_printed_at_fulltime,
    -- DATEDIFF(DAY, gorders_ranked.created_at, glogs_order_s2.created_at)         AS d_printed_at_date_t_since_order_at,
    order_date_helper(gorders_ranked.ship_at, 'hour')                        AS d_printed_at_hour,
    order_date_helper(gorders_ranked.ship_at, 'block')                       AS d_printed_at_block,
    DATE(gorders_ranked.ship_at)                                             AS d_printed_at_day,
    DATE_PART(WEEK,   gorders_ranked.ship_at)                                AS d_printed_at_week,
    DATE_PART(MONTH,  gorders_ranked.ship_at)                                AS d_printed_at_month,
    DATE_PART(YEAR,   gorders_ranked.ship_at)                                AS d_printed_at_year,
    DATE_PART(QTR,    gorders_ranked.ship_at)                                AS d_printed_at_quater,
    DATE_PART(DW,     gorders_ranked.ship_at)                                AS d_printed_at_day_of_week,
    DATE_PART(DAY,    gorders_ranked.ship_at)                                AS d_printed_at_day_of_month,
    DATE_PART(DOY,    gorders_ranked.ship_at)                                AS d_printed_at_day_of_year,
    gorders_ranked.ship_at                                                   AS d_printed_at_fulltime,
    DATEDIFF(DAY, gorders_ranked.created_at, gorders_ranked.ship_at)         AS d_printed_at_date_t_since_order_at,
    -- Chorun: tracking order
    gorders_ranked.tracking_order_s2_at,
    gorders_ranked.tracking_order_s3a_at,
    gorders_ranked.tracking_order_s3b_at,
    gorders_ranked.tracking_order_s3c_at,
    gorders_ranked.tracking_order_s3d_at,
    gorders_ranked.tracking_order_s3tc_at,
    gorders_ranked.tracking_order_s33_at,
    gorders_ranked.tracking_order_s4_at
FROM m_order
LEFT JOIN gorders_ranked          ON gorders_ranked.siren_id = m_order._id        AND gorders_ranked.ranked = 1
-- LEFT JOIN glogs_order_s2          ON glogs_order_s2.order_id = gorders_ranked._id AND glogs_order_s2.ranked = 1
)
