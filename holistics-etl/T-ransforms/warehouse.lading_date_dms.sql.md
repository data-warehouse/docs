# [warehouse.lading_date_dms](../holistics-etl/T-ransforms/warehouse.lading_date_dms.sql)

| **Field**                           | **Data Type**            | **Table Lake**                                               | **Description**                      | **Note**    |
| ----------------------------------- | ------------------------ | ------------------------------------------------------------ | ------------------------------------ | ----------- |
| id                                  | varchar(1020) (NOT NULL) | lake.morders                                                 | id lading_date_dms                   | 1           |
| d_ship_at_hour                      | varchar(256)             | lake.gorders                                                 | ship_at nằm trong khoảng giờ nào     | 2, 3, 8, 10 |
| d_ship_at_block                     | varchar(256)             | lake.gorders                                                 | ship_at nằm trong ca nào             | 2, 3, 8, 11 |
| d_ship_at_day                       | date                     | lake.gorders                                                 | ship_at là ngày nào                  | 2, 3, 8     |
| d_ship_at_week                      | int4                     | lake.gorders                                                 | ship_at là tuần nào trong năm        | 2, 3, 8     |
| d_ship_at_month                     | int4                     | lake.gorders                                                 | ship_at là tháng nào trong năm       | 2, 3, 8     |
| d_ship_at_year                      | int4                     | lake.gorders                                                 | ship_at là năm nào                   | 2, 3, 8     |
| d_ship_at_quater                    | int4                     | lake.gorders                                                 | ship_at là quý nào trong năm         | 2, 3, 8     |
| d_ship_at_day_of_week               | int4                     | lake.gorders                                                 | ship_at là ngày nào trong tuần       | 2, 3, 8     |
| d_ship_at_day_of_month              | int4                     | lake.gorders                                                 | ship_at là ngày nào trong tháng      | 2, 3, 8     |
| d_ship_at_day_of_year               | int4                     | lake.gorders                                                 | ship_at là ngày nào trong năm        | 2, 3, 8     |
| d_ship_at_fulltime                  | timestamp                | lake.gorders                                                 | ship_at                              | 2, 3, 8     |
| d_ship_at_date_t_since_order_at     | int8                     | lake.gorders                                                 | ship_at cách order_at mấy ngày       | 2, 3, 4, 8  |
| d_order_at_hour                     | varchar(256)             | lake.gorders                                                 | order_at nằm trong khoảng giờ nào    | 2, 4, 8, 10 |
| d_order_at_block                    | varchar(256)             | lake.gorders                                                 | order_at nằm trong ca nào            | 2, 4, 8, 11 |
| d_order_at_day                      | date                     | lake.gorders                                                 | order_at là ngày nào                 | 2, 4, 8     |
| d_order_at_week                     | int4                     | lake.gorders                                                 | order_at là tuần nào trong năm       | 2, 4, 8     |
| d_order_at_month                    | int4                     | lake.gorders                                                 | order_at là tháng nào trong năm      | 2, 4, 8     |
| d_order_at_year                     | int4                     | lake.gorders                                                 | order_at là năm nào                  | 2, 4, 8     |
| d_order_at_quater                   | int4                     | lake.gorders                                                 | order_at là quý nào trong năm        | 2, 4, 8     |
| d_order_at_day_of_week              | int4                     | lake.gorders                                                 | order_at là ngày nào trong tuần      | 2, 4, 8     |
| d_order_at_day_of_month             | int4                     | lake.gorders                                                 | order_at là ngày nào trong tháng     | 2, 4, 8     |
| d_order_at_day_of_year              | int4                     | lake.gorders                                                 | order_at là ngày nào trong năm       | 2, 4, 8     |
| d_order_at_fulltime                 | timestamp                | lake.gorders                                                 | order_at                             | 2, 4, 8     |
| d_order_at_date_t_since_order_at    | int8                     | lake.gorders                                                 | order_at cách order_at mấy ngày      | 2, 4, 8     |
| d_received_at_hour                  | varchar(256)             | lake.gorders                                                 | received_at nằm trong khoảng giờ nào | 2, 5, 8, 10 |
| d_received_at_block                 | varchar(256)             | lake.gorders                                                 | received_at nằm trong ca nào         | 2, 5, 8, 11 |
| d_received_at_day                   | date                     | lake.gorders                                                 | received_at là ngày nào              | 2, 5, 8     |
| d_received_at_week                  | int4                     | lake.gorders                                                 | received_at là tuần nào trong năm    | 2, 5, 8     |
| d_received_at_month                 | int4                     | lake.gorders                                                 | received_at là tháng nào trong năm   | 2, 5, 8     |
| d_received_at_year                  | int4                     | lake.gorders                                                 | received_at là năm nào               | 2, 5, 8     |
| d_received_at_quater                | int4                     | lake.gorders                                                 | received_at là quý nào trong năm     | 2, 5, 8     |
| d_received_at_day_of_week           | int4                     | lake.gorders                                                 | received_at là ngày nào trong tuần   | 2, 5, 8     |
| d_received_at_day_of_month          | int4                     | lake.gorders                                                 | received_at là ngày nào trong tháng  | 2, 5, 8     |
| d_received_at_day_of_year           | int4                     | lake.gorders                                                 | received_at là ngày nào trong năm    | 2, 5, 8     |
| d_received_at_fulltime              | timestamp                | lake.gorders                                                 | received_at                          | 2, 5, 8     |
| d_received_at_date_t_since_order_at | int8                     | lake.gorders                                                 | received_at cách order_at mấy ngày   | 2, 4, 5, 8  |
| d_estimate_at_hour                  | varchar(256)             | lake.gorders                                                 | estimate_at nằm trong khoảng giờ nào | 2, 6, 8, 10 |
| d_estimate_at_block                 | varchar(256)             | lake.gorders                                                 | estimate_at nằm trong ca nào         | 2, 6, 8, 11 |
| d_estimate_at_day                   | date                     | lake.gorders                                                 | estimate_at là ngày nào              | 2, 6, 8     |
| d_estimate_at_week                  | int4                     | lake.gorders                                                 | estimate_at là tuần nào trong năm    | 2, 6, 8     |
| d_estimate_at_month                 | int4                     | lake.gorders                                                 | estimate_at là tháng nào trong năm   | 2, 6, 8     |
| d_estimate_at_year                  | int4                     | lake.gorders                                                 | estimate_at là năm nào               | 2, 6, 8     |
| d_estimate_at_quater                | int4                     | lake.gorders                                                 | estimate_at là quý nào trong năm     | 2, 6, 8     |
| d_estimate_at_day_of_week           | int4                     | lake.gorders                                                 | estimate_at là ngày nào trong tuần   | 2, 6, 8     |
| d_estimate_at_day_of_month          | int4                     | lake.gorders                                                 | estimate_at là ngày nào trong tháng  | 2, 6, 8     |
| d_estimate_at_day_of_year           | int4                     | lake.gorders                                                 | estimate_at là ngày nào trong năm    | 2, 6, 8     |
| d_estimate_at_fulltime              | timestamp                | lake.gorders                                                 | estimate_at                          | 2, 6, 8     |
| d_estimate_at_date_t_since_order_at | int8                     | lake.gorders                                                 | estimate_at cách order_at mấy ngày   | 2, 4, 6, 8  |
| d_printed_at_hour                   | varchar(256)             | lake.gorders<br>lake.glogs                                   | printed_at nằm trong khoảng giờ nào  | 2, 7, 9, 10 |
| d_printed_at_block                  | varchar(256)             | lake.gorders<br>lake.glogs                                   | printed_at nằm trong ca nào          | 2, 7, 9, 11 |
| d_printed_at_day                    | date                     | lake.gorders<br>lake.glogs                                   | printed_at là ngày nào               | 2, 7, 9     |
| d_printed_at_week                   | int4                     | lake.gorders<br>lake.glogs                                   | printed_at là tuần nào trong năm     | 2, 7, 9     |
| d_printed_at_month                  | int4                     | lake.gorders<br>lake.glogs                                   | printed_at là tháng nào trong năm    | 2, 7, 9     |
| d_printed_at_year                   | int4                     | lake.gorders<br>lake.glogs                                   | printed_at là năm nào                | 2, 7, 9     |
| d_printed_at_quater                 | int4                     | lake.gorders<br>lake.glogs                                   | printed_at là quý nào trong năm      | 2, 7, 9     |
| d_printed_at_day_of_week            | int4                     | lake.gorders<br>lake.glogs                                   | printed_at là ngày nào trong tuần    | 2, 7, 9     |
| d_printed_at_day_of_month           | int4                     | lake.gorders<br>lake.glogs                                   | printed_at là ngày nào trong tháng   | 2, 7, 9     |
| d_printed_at_day_of_year            | int4                     | lake.gorders<br>lake.glogs                                   | printed_at là ngày nào trong năm     | 2, 7, 9     |
| d_printed_at_fulltime               | timestamp                | lake.gorders<br>lake.glogs                                   | printed_at                           | 2, 7, 9     |
| d_printed_at_date_t_since_order_at  | int8                     | lake.gorders<br>lake.glogs                                   | printed_at cách order_at mấy ngày    | 2, 4, 7, 9  |
| tracking_order_s2_at                | timestamp                | lake.gorders<br>lake.gorder_trackings<br>lake.gpartner_statuses | tracking đơn hàng tại S2             | 12, 13      |
| tracking_order_s3a_at               | timestamp                | lake.gorders<br>lake.gorder_trackings<br>lake.gpartner_statuses | tracking đơn hàng tại S3A            | 12, 13      |
| tracking_order_s3b_at               | timestamp                | lake.gorders<br>lake.gorder_trackings<br>lake.gpartner_statuses | tracking đơn hàng tại S3B            | 12, 13      |
| tracking_order_s3c_at               | timestamp                | lake.gorders<br>lake.gorder_trackings<br>lake.gpartner_statuses | tracking đơn hàng tại S3C            | 12, 13      |
| tracking_order_s3d_at               | timestamp                | lake.gorders<br>lake.gorder_trackings<br>lake.gpartner_statuses | tracking đơn hàng tại S3D            | 12, 13      |
| tracking_order_s3tc_at              | timestamp                | lake.gorders<br>lake.gorder_trackings<br>lake.gpartner_statuses | tracking đơn hàng tại S3TC           | 12, 13      |
| tracking_order_s33_at               | timestamp                | lake.gorders<br>lake.gorder_trackings<br>lake.gpartner_statuses | tracking đơn hàng tại S3.3           | 12, 13      |
| tracking_order_s4_at                | timestamp                | lake.gorders<br>lake.gorder_trackings<br>lake.gpartner_statuses | tracking đơn hàng tại S4             | 12, 13      |

**Note:**

1. *id lading_date_dms* được định nghĩa là id của *lake.morders*
2. timezone : *Asia/Ho_Chi_Minh*
3. *ship_at* là thời điểm đơn COD được gửi cho đối tác giao vận
4. *order_at* là thời điểm tạo đơn COD thành công
5. *received_at* là thời điểm đối tác giao vận xác nhận đã giao đơn COD thành công cho khách hàng
6. *estimate_at* là thời điểm ước lượng giao đơn COD tới khách hàng (tùy theo vị trí địa lý)
7. *printed_at* là thời điểm đơn COD đã xác nhận thông tin hợp lệ, có thông tin đối tác giao vận kèm theo để chuẩn bị gửi (trạng thái S2 trên hệ thống vận đơn)
8. nếu có nhiều hơn 1 đơn COD thì thời điểm ghi nhận là thời điểm đầu tiên ghi nhận được
9. nếu có nhiều hơn 1 đơn COD thì thời điểm ghi nhận là thời điểm cuối cùng ghi nhận được
10. format output: *hour + '_' + (hour+1)* , với *hour* là thời điểm quy đổi ra giờ nào trong ngày
11. format output: *'0h - 12h', '12h - 18h', '18h - 24h'*  , khi thời điểm quy đổi ra giờ trong ngày nằm trong khoảng thời gian tương ứng
12. *tracking đơn hàng* được định nghĩa là thời điểm thay đổi trạng thái vận đơn của các đơn hàng COD 
13. (update 01/04/2018) thông tin đơn hàng vận đơn bổ sung các level vận đơn mới thay thế các level vận đơn cũ (**S1** -> S1; **S2** -> S2; **S3** -> S3A, S3A, S3B, S3C, S3D, S3TC, S3.3; **S4** -> S4A, S4B)
