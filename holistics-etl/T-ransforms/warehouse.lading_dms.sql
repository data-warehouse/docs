/* warehouse.lading_dms */

WITH
transactions_ranked AS (
    SELECT
        transactions.*,
        (CASE   WHEN transactions.status = 'success'    THEN    1
                WHEN transactions.status = 'pending'    THEN    2
                WHEN transactions.status = 'failed'     THEN    3
                WHEN transactions.status = 'canceled'   THEN    4
                ELSE 5
        END) AS status_ranked,
        ROW_NUMBER() OVER (ORDER BY status_ranked ASC) AS ranked
    FROM lake.transactions
)
(SELECT
    gorders.cod_code                                                                    AS lg_cod_code,
    parcel_partners.name                                                                AS lg_partner_name,
    parcel_partners.code                                                                AS lg_partner_code,
    gorders.address                                                                     AS lg_address,
    provinces.name                                                                      AS lg_province,
    gorders.district                                                                    AS lg_district,
    CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, gorders.received_at))       AS lg_received_at,
    CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, gorders.ship_at))           AS lg_ship_at,
    DATE(CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, gorders.created_at)))  AS lg_order_at,
    CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, gorders.received_estimate)) AS lg_estimate_time,
    gusers.email                                                                        AS lg_lading_employee_name,
    gorders.name                                                                        AS lg_customer_name,
    gorders.mobile                                                                      AS lg_customer_mobile,
    gorders.s_level                                                                     AS lg_level_lading,
    gorders.sub_level                                                                   AS lg_sub_level_lading,
    (CASE CAST(gorders.sub_level AS INTEGER)
        WHEN 0 THEN 'Chờ xử lý'
        WHEN 1 THEN 'Tiếp tục chăm sóc'
        WHEN 2 THEN 'Dừng chăm sóc'
    END)                                                                                AS lg_status_lading,
    CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, gorders.day_receive))       AS lg_expected_at,
    gorders.course_id                                                                   AS lg_course_id,
    gorders.course_name                                                                 AS lg_course_name,
    gorders.lading_code                                                                 AS lg_lading_code,
    normalize_price(gorders.course_price)                                               AS lg_price,
    normalize_price(gorders.promotion_price)                                            AS lg_real_price,
    (CASE gorders.is_checking WHEN 'true' THEN 'Đã đối soát' ELSE 'Chưa đối soát' END)  AS lg_checking_lading,
    gorders.email                                                                       AS lg_customer_email,
    gorders._id                                                                         AS lg_lading_id,
    gorders.orderer                                                                     AS lg_orderer,
    gorders.creater                                                                     AS lg_creater
FROM lake.gorders
LEFT JOIN lake.parcel_partners ON parcel_partners._id = gorders.partner_id
LEFT JOIN lake.gusers          ON gusers._id          = gorders.user_id
LEFT JOIN lake.provinces       ON provinces.code      = gorders.province
) UNION (
SELECT
    tr.cod_code                                                                         AS lg_cod_code,
    NULL                                                                                AS lg_partner_name,
    NULL                                                                                AS lg_partner_code,
    bbuyers.address                                                                     AS lg_address,
    NULL                                                                                AS lg_province,
    NULL                                                                                AS lg_district,
    CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, tr.received_at))            AS lg_received_at,
    NULL                                                                                AS lg_ship_at,
    NULL                                                                                AS lg_order_at,
    NULL                                                                                AS lg_estimate_time,
    NULL                                                                                AS lg_lading_employee_name,
    NULL                                                                                AS lg_customer_name,
    NULL                                                                                AS lg_customer_mobile,
    NULL                                                                                AS lg_level_lading,
    NULL                                                                                AS lg_sub_level_lading,
    NULL                                                                                AS lg_status_lading,
    CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, transaction_details.expected_date)) AS lg_expected_at,
    bcourses.id_edumall                                                                 AS lg_course_id,
    bcourses.name                                                                       AS lg_course_name,
    tr.gateway_lading_code                                                              AS lg_lading_code,
    tr.price                                                                            AS lg_price,
    tr.price                                                                            AS lg_real_price,
    NULL                                                                                AS lg_checking_lading,
    bbuyers.email                                                                       AS lg_customer_email,
    ('dir_' || ISNULL(CAST(tr.buyer_id AS VARCHAR), 'unknown') || '.' || ISNULL(CAST(bcourses.id AS VARCHAR), 'unknown')) AS lg_lading_id,
    tr.orderer_email                                                                    AS lg_orderer,
    NULL                                                                                AS lg_creater
FROM transactions_ranked AS tr
LEFT JOIN lake.bbuyers              ON bbuyers.id = tr.buyer_id
LEFT JOIN lake.bcourses             ON bcourses._transaction_id = tr.id
LEFT JOIN lake.transaction_details  ON transaction_details._transaction_id = tr.id
                                    AND tr.status_ranked = 1
WHERE UPPER(tr.method) = 'GATEWAY'
)