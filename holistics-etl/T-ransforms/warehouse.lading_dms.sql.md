# [warehouse.lading_dms](../holistics-etl/T-ransforms/warehouse.lading_dms.sql)

| **Field**               | **Data Type**            | **Table Lake**                                       | **Description**                                              | **Note** |
| ----------------------- | ------------------------ | ---------------------------------------------------- | ------------------------------------------------------------ | -------- |
| lg_cod_code             | varchar(1020)            | lake.gorders<br/>lake.transactions                   | mã cod                                                       | 7        |
| lg_partner_name         | varchar(1020)            | lake.parcel_partners                                 | tên đối tác giao vận                                         | 2        |
| lg_partner_code         | varchar(1020)            | lake.parcel_partners                                 | mã đối tác giao vận                                          | 2        |
| lg_address              | varchar(1020)            | lake.gorders<br/>lake.bbuyers                        | địa chỉ                                                      | 3, 7     |
| lg_province             | varchar(255)             | lake.provinces                                       | tỉnh                                                         | 3        |
| lg_district             | varchar(1020)            | lake.gorders                                         | quận/huyện                                                   | 3        |
| lg_received_at          | timestamp                | lake.gorders                                         | thời điểm phát thành công                                    | 4        |
| lg_ship_at              | timestamp                | lake.gorders                                         | thời điểm giao đơn đi đối tác                                | 4        |
| lg_order_at             | date                     | lake.gorders                                         | thời điểm tạo đơn                                            | 4        |
| lg_estimate_time        | timestamp                | lake.gorders                                         | ngày dự kiến giao hàng sau khi chuyển cho đối tác thành công | 4        |
| lg_lading_employee_name | varchar(1020)            | lake.gusers                                          | email nhân viên                                              |          |
| lg_customer_name        | varchar(1020)            | lake.gorders                                         | tên người nhận                                               | 3        |
| lg_customer_mobile      | varchar(1020)            | lake.gorders                                         | số điện thoại người nhận                                     | 3        |
| lg_level_lading         | varchar(1020)            | lake.gorders                                         | trạng thái hệ thống psmo                                     |          |
| lg_sub_level_lading     | varchar(1020)            | lake.gorders                                         | trạng thái vận đơn (dạng số)                                 | 5        |
| lg_status_lading        | varchar(23)              | lake.gorders                                         | trạng thái vận đơn (dạng chữ)                                | 5        |
| lg_expected_at          | timestamp                | lake.gorders<br/>lake.transaction_details            | thời gian dự kiến giao thành công                            | 4, 7     |
| lg_course_id            | varchar(1020)            | lake.gorders<br/>lake.bcourses                       | mã khóa học                                                  | 7        |
| lg_course_name          | varchar(1020)            | lake.gorders<br/>lake.bcourses                       | tên khóa học                                                 | 7        |
| lg_lading_code          | varchar(1020)            | lake.gorders<br/>lake.transactions                   | mã vận đơn                                                   | 7        |
| lg_price                | int4                     | lake.gorders<br/>lake.transactions                   | giá gốc                                                      | 7        |
| lg_real_price           | int4                     | lake.gorders<br/>lake.transactions                   | giá thực thu                                                 | 7        |
| lg_checking_lading      | varchar(18)              | lake.gorders                                         | đã đối soát hay chưa                                         | 6, 7     |
| lg_customer_email       | varchar(1020)            | lake.gorders<br/>lake.bbuyers                        | email người nhận                                             | 3, 7     |
| lg_lading_id            | varchar(1020) (NOT NULL) | lake.gorders<br/>lake.transactions<br/>lake.bcourses | id lading_dms                                                | 1, 7, 8  |
| lg_orderer              | varchar(1020)            | lake.gorders                                         | người đặt hàng                                               |          |
| lg_creater              | varchar(1020)            | lake.gorders                                         | người tạo đơn                                                |          |

**Note:**

1. *id lading_dms* được định nghĩa là id của *lading_dms*
2. thông tin đối tác giao vận
3. thông tin người nhận
4. timezone : *Asia/Ho_Chi_Minh*
5. quy đổi dạng số-dạng chữ của trạng thái vận đơn: *0-'Chờ xử lý', 1-'Tiếp tục chăm sóc', 2-'Dừng chăm sóc'*
6. format: *'Đã đối soát', 'Chưa đối soát'*
7. với những giao dịch gateway *tr.method = 'GATEWAY'*, thông tin vận đơn lấy ở dữ liệu thanh toán
8. với những giao dịch gateway *lg_lading_id* được lấy theo thông tin thanh toán với cấu trúc:   *'dir_' + lake.transactions.buyer_id + lake.bcourses.id*

