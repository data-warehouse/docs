# [warehouse.lading_employee_dms](../holistics-etl/T-ransforms/warehouse.lading_employee_dms.sql)

| **Field** | **Data Type**            | **Table Lake** | **Description**         | **Note** |
| --------- | ------------------------ | -------------- | ----------------------- | -------- |
| le_name   | varchar(1020)            | lake.gusers    | tên nhân viên           | 2        |
| le_email  | varchar(1020)            | lake.gusers    | email nhân viên         | 2        |
| le_mobile | varchar(1020)            | lake.gusers    | số điện thoại nhân viên | 2        |
| le_id     | varchar(1020) (NOT NULL) | lake.gusers    | id lading_employee_dms  | 1        |

**Note:**

1. *id lading_employee_dms* được định nghĩa là id của bảng *lake.gusers*
2. thông tin nhân viên trên hệ thống psmo