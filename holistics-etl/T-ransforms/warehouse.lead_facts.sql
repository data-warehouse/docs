/* warehouse.lead_facts */

(
-- C3 facts
SELECT
    1 AS quantity,
    COALESCE(mar_dms._id, eros_dms._id) AS lf_order_id,
    mar_dms.cost AS lf_cost,
    mar_dms.price AS lf_price,
    mar_dms.promotion_price AS lf_promotion_price,
    --DEPRECATED: eros_dms.latest_lading_price AS lf_final_money,
    eros_dms.latest_transaction_price AS lf_final_money,
    eros_dms.latest_lading_fee AS lf_lading_fee,
    -- eros_dms.payment_fee AS lf_payment_fee,
    eros_dms.call_logs_count AS lf_total_call,
    --PENDING: AS lf_status,
    --PENDING: AS lf_bi_start_time,
    --PENDING: AS lf_bi_end_time,
    --PENDING: AS lf_bi_status,
    DATEDIFF(MIN, eros_dms.created_at, LEAST(eros_dms.l8a_at, eros_dms.l8b_at)) AS lf_l8_at,
    DATEDIFF(MIN, eros_dms.created_at, LEAST(eros_dms.l7a_at, eros_dms.l7b_at)) AS lf_l7_at,
    DATEDIFF(MIN, eros_dms.created_at, LEAST(eros_dms.l4a_at, eros_dms.l4b_at)) AS lf_l4_at,
    DATEDIFF(MIN, eros_dms.created_at, LEAST(eros_dms.l3a_at, eros_dms.l3b_at)) AS lf_l3_at,
    DATEDIFF(MIN, eros_dms.created_at, LEAST(eros_dms.l2_at , eros_dms.l2x_at)) AS lf_l2_at,
    DATEDIFF(MIN, eros_dms.created_at,                           eros_dms.l1_at)   AS lf_l1_at,
    --CANCELLED: AS lf_c3_at,
    (CASE WHEN mar_dms._id IS NOT NULL                                       THEN 1 ELSE 0 END) AS lf_is_c1,
    (CASE WHEN mar_dms._id IS NOT NULL                                       THEN 1 ELSE 0 END) AS lf_is_c2,
    (CASE WHEN mar_dms._id IS NOT NULL                                       THEN 1 ELSE 0 END) AS lf_is_c3,
    (CASE WHEN CONVERT(INTEGER, SUBSTRING(eros_dms.level FROM 2 FOR 1)) >= 1 THEN 1 ELSE 0 END) AS lf_is_l1,
    (CASE WHEN CONVERT(INTEGER, SUBSTRING(eros_dms.level FROM 2 FOR 1)) >= 2 THEN 1 ELSE 0 END) AS lf_is_l2,
    (CASE WHEN CONVERT(INTEGER, SUBSTRING(eros_dms.level FROM 2 FOR 1)) >= 3 THEN 1 ELSE 0 END) AS lf_is_l3,
    (CASE WHEN CONVERT(INTEGER, SUBSTRING(eros_dms.level FROM 2 FOR 1)) >= 4 THEN 1 ELSE 0 END) AS lf_is_l4,
    (CASE WHEN CONVERT(INTEGER, SUBSTRING(eros_dms.level FROM 2 FOR 1)) >= 7 THEN 1 ELSE 0 END) AS lf_is_l7,
    (CASE WHEN CONVERT(INTEGER, SUBSTRING(eros_dms.level FROM 2 FOR 1))  = 8 THEN 1 ELSE 0 END) AS lf_is_l8,
    (CASE WHEN eros_dms.level = 'L2X' THEN 1 ELSE 0 END) AS lf_is_l2x,
    (CASE WHEN eros_dms.level = 'L3A' THEN 1 ELSE 0 END) AS lf_is_l3a,
    (CASE WHEN eros_dms.level = 'L3B' THEN 1 ELSE 0 END) AS lf_is_l3b,
    (CASE WHEN eros_dms.level = 'L4A' THEN 1 ELSE 0 END) AS lf_is_l4a,
    (CASE WHEN eros_dms.level = 'L4B' THEN 1 ELSE 0 END) AS lf_is_l4b,
    (CASE WHEN eros_dms.level = 'L7A' THEN 1 ELSE 0 END) AS lf_is_l7a,
    (CASE WHEN eros_dms.level = 'L7B' THEN 1 ELSE 0 END) AS lf_is_l7b,
    (CASE WHEN eros_dms.level = 'L8A' THEN 1 ELSE 0 END) AS lf_is_l8a,
    (CASE WHEN eros_dms.level = 'L8B' THEN 1 ELSE 0 END) AS lf_is_l8b,
    /* -- FOREIGN KEYS -- */
    COALESCE(mar_dms._id, eros_dms._id) AS product_dm_id,
    COALESCE(mar_dms._id, eros_dms._id) AS date_dm_id,
    COALESCE(CASE WHEN CONVERT(INTEGER, SUBSTRING(eros_dms.level FROM 2 FOR 1)) <  3 THEN eros_dms.l1_staff_id END, 'notavailable') AS telemar_dm_id,
    COALESCE(CASE WHEN CONVERT(INTEGER, SUBSTRING(eros_dms.level FROM 2 FOR 1)) >= 3 THEN eros_dms.staff_id    END, 'notavailable') AS telesale_dm_id,
    --TODO: AS landing_page_dm_id,
    --TODO: AS price_book_dm_id,
    COALESCE(CAST(mar_dms.ad_bydate_id AS VARCHAR), 'not_found_mar_dms_adbydate') AS ads_dm_id,
    --TODO: AS marketter_dm_id,
    --TODO: AS industry_dm_id,
    COALESCE(mar_dms._id, eros_dms._id) AS customer_dm_id,
    COALESCE(eros_dms.latest_lading_id, 'notavailable') AS lading_dm_id,
    --TODO: AS partner_lading_dm_id,
    --TODO: AS location_dm_id,
    --TODO: AS level_product_dm_id,
    --TODO: AS level_lading_dm_id,
    --TODO: AS manager_sale_dm_id,
    --TODO: AS manager_sale_block_dm_id,
    --TODO: AS manager_marketer_dm_id,
    --TODO: AS source_product_dm_id,
    COALESCE(eros_dms.latest_lading_employee_id, 'notavailable') AS lading_employee_dm_id,
    --TODO: AS category_course_dm_id,
    --TODO: AS channel_ad_dm_id,
    --TODO: AS account_ad_dm_id
    COALESCE(eros_dms.latest_lading_id, eros_dms._id, 'notavailable') AS level_dm_id,
    COALESCE(eros_dms._id, 'notavailable') AS order_date_dm_id,
    COALESCE(eros_dms._id, 'notavailable') AS order_sale_dm_id,
    COALESCE(eros_dms._id, 'notavailable') AS lading_date_dm_id,
    COALESCE(CAST(eros_dms.latest_transaction_id AS VARCHAR), 'notavailable') AS payment_dm_id,
    COALESCE(eros_dms._id, 'notavailable') AS payment_checker_dm_id,
    COALESCE(eros_dms.course_id, mar_dms.course_id, 'notavailable') AS course_dm_id,
    CAST(mar_dms.total_rate AS REAL) AS total_rate,
    NULL AS edumall_payment_dm_id
    -- eros_dms.user_id as edumall_user_dm_id
FROM staging.mar_dms
FULL OUTER JOIN staging.eros_dms
ON eros_dms._id = mar_dms._id
) UNION ALL (
-- C2, C1 facts
SELECT
    ISNULL(ad_bydates.c1, 0) AS quantity,
    ('c12_' || CAST(ad_bydates.id AS VARCHAR)) AS lf_order_id,
    ad_bydates.cost AS lf_cost,
    NULL AS lf_price,
    NULL AS lf_promotion_price,
    --DEPRECATED: eros_dms.latest_lading_price AS lf_final_money,
    NULL AS lf_final_money,
    NULL AS lf_lading_fee,
    -- eros_dms.payment_fee AS lf_payment_fee,
    NULL AS lf_total_call,
    --PENDING: AS lf_status,
    --PENDING: AS lf_bi_start_time,
    --PENDING: AS lf_bi_end_time,
    --PENDING: AS lf_bi_status,
    NULL AS lf_l8_at,
    NULL AS lf_l7_at,
    NULL AS lf_l4_at,
    NULL AS lf_l3_at,
    NULL AS lf_l2_at,
    NULL AS lf_l1_at,
    --CANCELLED: AS lf_c3_at,
    ISNULL(ad_bydates.c1, 0) AS lf_is_c1,
    ISNULL(ad_bydates.c2, 0) AS lf_is_c2,
    0 AS lf_is_c3,
    0 AS lf_is_l1,
    0 AS lf_is_l2,
    0 AS lf_is_l3,
    0 AS lf_is_l4,
    0 AS lf_is_l7,
    0 AS lf_is_l8,
    0 AS lf_is_l2x,
    0 AS lf_is_l3a,
    0 AS lf_is_l3b,
    0 AS lf_is_l4a,
    0 AS lf_is_l4b,
    0 AS lf_is_l7a,
    0 AS lf_is_l7b,
    0 AS lf_is_l8a,
    0 AS lf_is_l8b,
    /* -- FOREIGN KEYS -- */
    'notavailable' AS product_dm_id,
    CAST(ad_bydates.id AS VARCHAR)  AS date_dm_id,
    'notavailable' AS telemar_dm_id,
    'notavailable' AS telesale_dm_id,
    --TODO: AS landing_page_dm_id,
    --TODO: AS price_book_dm_id,
    CAST(ad_bydates.id AS VARCHAR) AS ads_dm_id,
    --TODO: AS marketter_dm_id,
    --TODO: AS industry_dm_id,
    'notavailable' AS customer_dm_id,
    'notavailable' AS lading_dm_id,
    --TODO: AS partner_lading_dm_id,
    --TODO: AS location_dm_id,
    --TODO: AS level_product_dm_id,
    --TODO: AS level_lading_dm_id,
    --TODO: AS manager_sale_dm_id,
    --TODO: AS manager_sale_block_dm_id,
    --TODO: AS manager_marketer_dm_id,
    --TODO: AS source_product_dm_id,
    'notavailable' AS lading_employee_dm_id,
    --TODO: AS category_course_dm_id,
    --TODO: AS channel_ad_dm_id,
    --TODO: AS account_ad_dm_id
    'notavailable' AS level_dm_id,
    'notavailable' AS order_date_dm_id,
    'notavailable' AS order_sale_dm_id,
    'notavailable' AS lading_date_dm_id,
    'notavailable' AS payment_dm_id,
    'notavailable' AS payment_checker_dm_id,
    mar_courses.source_id AS course_dm_id,
    NULL AS total_rate,
    'notavailable' AS edumall_payment_dm_id
    -- 'notavailable' as edumall_user_dm_id
FROM lake.ad_bydates
LEFT JOIN lake.ads              ON  ads.id = ad_bydates.ad_id
                                AND ads.deleted_at IS NULL
LEFT JOIN lake.ad_groups        ON  ad_groups.id = ads.ad_group_id
                                AND ad_groups.deleted_at IS NULL
LEFT JOIN lake.campaigns        ON  campaigns.id = ad_groups.campaign_id
                                AND campaigns.deleted_at IS NULL
LEFT JOIN lake.channel_courses  ON  channel_courses.id = campaigns.channel_course_id
                                AND channel_courses.deleted_at IS NULL
LEFT JOIN lake.mar_courses      ON  mar_courses.id = channel_courses.course_id
                                AND mar_courses.deleted_at IS NULL
WHERE c3 IS NULL OR c3 = 0
) UNION (
-- Direct (Go straight to system without advertising (nami) or sale convert (eros))
WITH
transactions_ranked AS (
    SELECT
        transactions.*,
        -- ROW_NUMBER() OVER (PARTITION BY transactions.buyer_id, bcourses.id ORDER BY transactions.created_at DESC) AS ranked
        (CASE   WHEN transactions.status = 'success'    THEN    1
                WHEN transactions.status = 'pending'    THEN    2
                WHEN transactions.status = 'failed'     THEN    3
                WHEN transactions.status = 'canceled'   THEN    4
                ELSE 5
        END) AS status_ranked,
        ROW_NUMBER() OVER (PARTITION BY transactions.buyer_id, bcourses.id ORDER BY status_ranked ASC) AS ranked
    FROM lake.transactions
    LEFT JOIN lake.bcourses ON bcourses._transaction_id = transactions.id
)
SELECT
    1 AS quantity,
    ('dir_' || ISNULL(CAST(tr.buyer_id AS VARCHAR), 'unknown') || '.' || ISNULL(CAST(bcourses.id AS VARCHAR), 'unknown')) AS lf_order_id,
    SUM(tr.cost) AS lf_cost,
    SUM(bcourses.price) AS lf_price,
    bcourses.price AS lf_promotion_price,
    --DEPRECATED: eros_dms.latest_lading_price AS lf_final_money,
    --COALESCE(SUM(tr.price ), bcourses.price) AS lf_final_money,
    -- (CASE WHEN  (bcourses.price != NULL AND bcourses.price > 0) THEN bcourses.price  ELSE tr.price END) AS lf_final_money,
    (CASE   WHEN  (bcourses.price IS NULL) THEN tr.price
            WHEN  (bcourses.price <= tr.price) THEN bcourses.price
            ELSE  tr.price
    END) AS lf_final_money,
    NULL AS lf_lading_fee,
    -- eros_dms.payment_fee AS lf_payment_fee,
    NULL AS lf_total_call,
    --PENDING: AS lf_status,
    --PENDING: AS lf_bi_start_time,
    --PENDING: AS lf_bi_end_time,
    --PENDING: AS lf_bi_status,
    DATEDIFF(MIN, CONVERT(TIMESTAMP, MIN(tr.created_at)), CONVERT(TIMESTAMP, MIN(tr.received_at))) AS lf_l8_at,
    NULL AS lf_l7_at,
    NULL AS lf_l4_at,
    NULL AS lf_l3_at,
    NULL AS lf_l2_at,
    NULL AS lf_l1_at,
    --CANCELLED: AS lf_c3_at,
    0 AS lf_is_c1,
    0 AS lf_is_c2,
    0 AS lf_is_c3,
    0 AS lf_is_l1,
    0 AS lf_is_l2,
    0 AS lf_is_l3,
    0 AS lf_is_l4,
    0 AS lf_is_l7,
    (CASE WHEN UPPER(MAX(tr.status)) = 'SUCCESS' THEN 1 ELSE 0 END) AS lf_is_l8,
    0 AS lf_is_l2x,
    0 AS lf_is_l3a,
    0 AS lf_is_l3b,
    0 AS lf_is_l4a,
    0 AS lf_is_l4b,
    0 AS lf_is_l7a,
    0 AS lf_is_l7b,
    0 AS lf_is_l8a,
    (CASE WHEN UPPER(MAX(tr.status)) = 'SUCCESS' THEN 1 ELSE 0 END) AS lf_is_l8b,
    /* -- FOREIGN KEYS -- */
    ('dir_' || ISNULL(CAST(tr.buyer_id AS VARCHAR), 'unknown') || '.' || ISNULL(CAST(bcourses.id AS VARCHAR), 'unknown')) AS product_dm_id,
    ('dir_' || ISNULL(CAST(tr.buyer_id AS VARCHAR), 'unknown') || '.' || ISNULL(CAST(bcourses.id AS VARCHAR), 'unknown')) AS date_dm_id,
    'notavailable' AS telemar_dm_id,
    'notavailable' AS telesale_dm_id,
    --TODO: AS landing_page_dm_id,
    --TODO: AS price_book_dm_id,
        COALESCE(CAST(origin_transactions.ad_id AS VARCHAR), 'not_found_origin_transactions_ad_id') AS ads_dm_id,
    --TODO: AS marketter_dm_id,
    --TODO: AS industry_dm_id,
    ('dir_' || ISNULL(CAST(tr.buyer_id AS VARCHAR), 'unknown') || '.' || ISNULL(CAST(bcourses.id AS VARCHAR), 'unknown')) AS customer_dm_id,
    ('dir_' || ISNULL(CAST(tr.buyer_id AS VARCHAR), 'unknown') || '.' || ISNULL(CAST(bcourses.id AS VARCHAR), 'unknown')) AS lading_dm_id,
    --TODO: AS partner_lading_dm_id,
    --TODO: AS location_dm_id,
    --TODO: AS level_product_dm_id,
    --TODO: AS level_lading_dm_id,
    --TODO: AS manager_sale_dm_id,
    --TODO: AS manager_sale_block_dm_id,
    --TODO: AS manager_marketer_dm_id,
    --TODO: AS source_product_dm_id,
    'notavailable' AS lading_employee_dm_id,
    --TODO: AS category_course_dm_id,
    --TODO: AS channel_ad_dm_id,
    --TODO: AS account_ad_dm_id
    'notavailable' AS level_dm_id,
    ('dir_' || ISNULL(CAST(tr.buyer_id AS VARCHAR), 'unknown') || '.' || ISNULL(CAST(bcourses.id AS VARCHAR), 'unknown')) AS order_date_dm_id,
    'notavailable' AS order_sale_dm_id,
    'notavailable' AS lading_date_dm_id,
    CAST(MAX(tr.id) AS VARCHAR) AS payment_dm_id,
    'notavailable' AS payment_checker_dm_id,
    (CASE WHEN MAX(tr.combo) IS NOT NULL THEN MAX(tr.combo) ELSE MAX(bcourses.id_edumall) END) AS course_dm_id,
    NULL AS total_rate,
    MAX(tr.edumall_payment_id) as edumall_payment_dm_id
    -- MAX(edumall_user._id) as edumall_user_dm_id
FROM transactions_ranked AS tr
LEFT JOIN lake.bbuyers ON bbuyers.id = tr.buyer_id
LEFT JOIN lake.bcourses ON bcourses._transaction_id = tr.id
--- LEFT JOIN Ads with payment not create in Minerva .
LEFT JOIN (
  SELECT
  origin_transactions.source_id as transaction_id, ads.id as ad_id
  FROM lake.ads
  LEFT JOIN lake.origin_transactions ON origin_transactions.ad_code = ads.utm_medium AND ads.deleted_at IS NULL
  WHERE origin_transactions.siren_id IS NULL
) as origin_transactions ON origin_transactions.transaction_id = tr.id

-- LEFT JOIN lake.edumall_user ON edumall_user.email = bbuyers.email
WHERE tr.ranked = 1 AND (
    (UPPER(tr.method) = 'COD' AND tr.siren_id IS NULL) OR
    (UPPER(tr.method) = 'GATEWAY' AND tr.siren_id IS NULL) OR
    -- Add if gateway has siren_id
    -- (UPPER(tr.method) = 'GATEWAY' AND UPPER(tr.source) = 'EOP_GATEWAY') OR
    (UPPER(tr.method) = 'CASH'    AND tr.siren_id IS NULL) OR
    (UPPER(tr.method) = 'TRANSFER'    AND tr.siren_id IS NULL) OR
    (UPPER(tr.source) = 'B2B') OR
    (UPPER(tr.source) = 'CHANNEL_ECOMMERCE')
) AND tr.id IS NOT NULL
GROUP BY tr.buyer_id, bcourses.id, bcourses.price, tr.price, origin_transactions.ad_id
)