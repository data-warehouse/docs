# [warehouse.lead_facts](../holistics-etl/T-ransforms/warehouse.lead_facts.sql)

| **Field**             | **Data Type**            | **Table Lake**                                               | **Description**                                        | **Note**   |
| --------------------- | ------------------------ | ------------------------------------------------------------ | ------------------------------------------------------ | ---------- |
| quantity              | int4 (NOT NULL)          | lake.ad_bydates                                              | số lượng bản ghi                                       | 1, 2, 3, 4 |
| lf_order_id           | varchar(1020) (NOT NULL) | staging.mar_dms<br/>staging.eros_dms<br/>staging.bifrost_dms<br/>lake.bcourses | id đơn hàng tại các database source                    | 1, 2, 3, 4 |
| lf_cost               | float4                   | staging.mar_dms<br>staging.bifrost_dms<br>lake.bcourses      | giá c3                                                 | 3, 4       |
| lf_price              | int4                     | staging.mar_dms<br>staging.bifrost_dms<br>lake.bcourses      | giá bán                                                | 3, 4       |
| lf_promotion_price    | int4                     | staging.mar_dms<br>staging.bifrost_dms<br>lake.bcourses      | giá khuyến mãi                                         | 3, 4       |
| lf_final_money        | float4                   | staging.eros_dms                                             | giá thực thu                                           |            |
| lf_lading_fee         | float4                   | staging.eros_dms                                             | phí vận đơn                                            |            |
| lf_total_call         | int8                     | staging.eros_dms                                             | tổng cuộc gọi                                          |            |
| lf_l8_at              | int8                     | staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses     | thời điểm lên L8 (hoặc C3)                             | 3          |
| lf_l7_at              | int8                     | staging.eros_dms                                             | thời điểm lên L7 (hoặc C3)                             |            |
| lf_l4_at              | int8                     | staging.eros_dms                                             | thời điểm lên L4 (hoặc C3)                             |            |
| lf_l3_at              | int8                     | staging.eros_dms                                             | thời điểm lên L3 (hoặc C3)                             |            |
| lf_l2_at              | int8                     | staging.eros_dms                                             | thời điểm lên L2 (hoặc C3)                             |            |
| lf_l1_at              | int8                     | staging.eros_dms                                             | thời điểm lên L1 (hoặc C3)                             |            |
| lf_is_c1              | int4                     | staging.eros_dms                                             | có phải contact c1 không                               | 1          |
| lf_is_c2              | int4                     | staging.eros_dms                                             | có phải contact c2 không                               | 1          |
| lf_is_c3              | int4                     | staging.eros_dms                                             | có phải contact c2 không                               |            |
| lf_is_l1              | int4                     | staging.eros_dms                                             | có phải đơn hàng L1 không                              |            |
| lf_is_l2              | int4                     | staging.eros_dms                                             | có phải đơn hàng L2 không                              |            |
| lf_is_l3              | int4                     | staging.eros_dms                                             | có phải đơn hàng L3 không                              |            |
| lf_is_l4              | int4                     | staging.eros_dms                                             | có phải đơn hàng L4 không                              |            |
| lf_is_l7              | int4                     | staging.eros_dms                                             | có phải đơn hàng L7 không                              |            |
| lf_is_l8              | int4                     | staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses     | có phải đơn hàng L8 không                              | 3          |
| lf_is_l2x             | int4                     | staging.eros_dms                                             | có phải đơn hàng L2X không                             |            |
| lf_is_l3a             | int4                     | staging.eros_dms                                             | có phải đơn hàng L3A không                             |            |
| lf_is_l3b             | int4                     | staging.eros_dms                                             | có phải đơn hàng L3B không                             |            |
| lf_is_l4a             | int4                     | staging.eros_dms                                             | có phải đơn hàng L4A không                             |            |
| lf_is_l4b             | int4                     | staging.eros_dms                                             | có phải đơn hàng L4B không                             |            |
| lf_is_l7a             | int4                     | staging.eros_dms                                             | có phải đơn hàng L7A không                             |            |
| lf_is_l7b             | int4                     | staging.eros_dms                                             | có phải đơn hàng L7B không                             |            |
| lf_is_l8a             | int4                     | staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses     | có phải đơn hàng L8A không                             | 3          |
| lf_is_l8b             | int4                     | staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses     | có phải đơn hàng L8B không                             | 3          |
| product_dm_id         | varchar(1020)            | staging.mar_dms<br>staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses | foreign key trỏ tới bảng warehouse.product_dms         | 2, 3, 4    |
| date_dm_id            | varchar(1020)            | staging.mar_dms<br>staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses | foreign key trỏ tới bảng warehouse.date_dms            | 1, 2, 3, 4 |
| telemar_dm_id         | varchar(1020)            | staging.mar_dms<br>staging.eros_dms                          | foreign key trỏ tới bảng warehouse.telemar_dms         | 4          |
| telesale_dm_id        | varchar(1020)            | staging.mar_dms<br>staging.eros_dms                          | foreign key trỏ tới bảng warehouse.telesale_dms        | 4          |
| ads_dm_id             | varchar(12)              | staging.mar_dms<br>staging.eros_dms                          | foreign key trỏ tới bảng warehouse.ads_dms             | 4          |
| customer_dm_id        | varchar(1020)            | staging.mar_dms<br>staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses | foreign key trỏ tới bảng warehouse.customer_dms        | 2, 3       |
| lading_dm_id          | varchar(1020)            | staging.mar_dms<br/>staging.eros_dms<br/>staging.bifrost_dms<br/>lake.bcourses | foreign key trỏ tới bảng warehouse.lading_dms          | 4          |
| lading_employee_dm_id | varchar(1020)            | staging.mar_dms<br>staging.eros_dms                          | foreign key trỏ tới bảng warehouse.lading_employee_dms | 4          |
| level_dm_id           | varchar(1020)            | staging.mar_dms<br>staging.eros_dms                          | foreign key trỏ tới bảng warehouse.level_dms           | 4          |
| order_date_dm_id      | varchar(1020)            | staging.mar_dms<br>staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses | foreign key trỏ tới bảng warehouse.order_date_dms      | 2, 3, 4    |
| order_sale_dm_id      | varchar(1020)            | staging.mar_dms<br>staging.eros_dms                          | foreign key trỏ tới bảng warehouse.order_sale_dms      | 4          |
| lading_date_dm_id     | varchar(1020)            | staging.mar_dms<br>staging.eros_dms                          | foreign key trỏ tới bảng warehouse.lading_date_dms     | 4          |
| payment_dm_id         | varchar(1020)            | staging.mar_dms<br>staging.eros_dms                          | foreign key trỏ tới bảng warehouse.payment_dms         | 4          |
| payment_checker_dm_id | varchar(1020)            | staging.mar_dms<br>staging.eros_dms                          | foreign key trỏ tới bảng warehouse.payment_checker_dms | 4          |
| course_dm_id          | varchar(1020)            | staging.mar_dms<br>staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses<br>lake.mar_courses | foreign key trỏ tới bảng warehouse.course_dms          | 3, 4       |
| total_rate            | float4                   | staging.mar_dms                                              | tỉ lệ chuyển đổi doanh thu                             | 4          |

**Note:**

1. khi không tìm thấy id từ *staging.eros_dms* do bản ghi là contact thì tìm thông tin trên *staging.mar_dms*. Với trường *lf_order_id*, khi không tìm thấy id từ *staging.eros_dms* sẽ tạo id dựa theo thông tin quảng cáo với cấu trúc: *'c12_' + lake.ad_bydates.id*

2. khi không tìm thấy id từ *staging.eros_dms* và *staging.mar_dms* sẽ lấy transaction_course_id của *staging.bifrost_dms* với cấu trúc:   *'dir_' + lake.transactions.buyer_id + lake.bcourses.id*

3. khi không tìm được thông tin trên contact c3 hoặc đơn hàng sẽ lấy thông tin trên *staging.bifrost_dms* với logic và lấy bản ghi theo thứ tự ưu tiên về trạng thái thanh toán của đơn hàng: *success -> pending -> failed -> canceled*

4. các bản ghi lấy từ database nami cần loại bỏ các bản ghi soft delete: **.deleted_at IS NULL*
