SELECT
    COALESCE(gorders._id, morders._id) as level_id,
    morders.level AS level_product,
    gorders.level AS level_cod,
    gorders.tracking_level AS level_tracking_cod
    SUBSTRING(morders.level FROM 1 FOR 2) AS level_product_parent
FROM lake.morders
LEFT JOIN lake.gorders
ON gorders.siren_id = morders._id
WHERE morders.flag_deleted IS NULL OR flag_deleted <> TRUE  -- Cleansing
