# [warehouse.level_dms](../holistics-etl/T-ransforms/warehouse.level_dms.sql)

| **Field**            | **Data Type**            | **Table Lake**               | **Description**                                          | **Note** |
| -------------------- | ------------------------ | ---------------------------- | -------------------------------------------------------- | -------- |
| id                   | varchar(1020) (NOT NULL) | lake.morders<br>lake.gorders | id level_dms                                             | 1, 2     |
| level_product        | varchar(1020)            | lake.morders                 | level đơn hàng trên eros                                 | 2        |
| level_cod            | varchar(1020)            | lake.gorders                 | level đơn hàng trên psmo                                 |          |
| level_product_parent | varchar(8)               | lake.morders                 | level đơn hàng trên eros (không phân biệt cod/ khác cod) | 2        |
| level_tracking_cod   | varchar(1020)            | lake.gorders                 | level tracking đơn hàng cod trên psmo                    |          |

**Note:**

1. *id level_dms* được định nghĩa là *id* của bảng *lake.morders* hoặc *id* của bảng *lake.gorders*
2. bảng *lake.morders* không bao gồm bản ghi của những đơn hàng bị xóa: *morders.flag_deleted <> TRUE*


