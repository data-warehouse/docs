# [warehouse.order_date_dms](../holistics-etl/T-ransforms/warehouse.order_date_dms.sql)

| **Field**                  | **Data Type**            | **Table Lake**                                           | **Description**                                    | **Note** |
| -------------------------- | ------------------------ | -------------------------------------------------------- | -------------------------------------------------- | -------- |
| id                         | varchar(1020) (NOT NULL) | staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses | id order_date_dms                                  | 1        |
| d_l1_hour                  | varchar(256)             | staging.eros_dms                                         | thời điểm lên L1 là giờ nào trong ngày             | 2        |
| d_l1_block                 | varchar(256)             | staging.eros_dms                                         | thời điểm lên L1 thuộc ca nào                      | 3        |
| d_l1_day                   | date                     | staging.eros_dms                                         | thời điểm lên L1 là ngày nào                       |          |
| d_l1_week                  | int4                     | staging.eros_dms                                         | thời điểm lên L1 là tuần nào trong năm             |          |
| d_l1_month                 | int4                     | staging.eros_dms                                         | thời điểm lên L1 là tháng nào trong năm            |          |
| d_l1_year                  | int4                     | staging.eros_dms                                         | thời điểm lên L1 là năm nào                        |          |
| d_l1_quater                | int4                     | staging.eros_dms                                         | thời điểm lên L1 là quý nào trong năm              |          |
| d_l1_day_of_week           | int4                     | staging.eros_dms                                         | thời điểm lên L1 là ngày nào trong tuần            |          |
| d_l1_day_of_month          | int4                     | staging.eros_dms                                         | thời điểm lên L1 là ngày nào trong tháng           |          |
| d_l1_day_of_year           | int4                     | staging.eros_dms                                         | thời điểm lên L1 là ngày nào trong năm             |          |
| d_l1_fulltime              | timestamp                | staging.eros_dms                                         | thời điểm lên L1                                   |          |
| d_l1_date_t_since_order_at | int8                     | staging.eros_dms                                         | thời điểm lên L1 cách ngày đăng ký bao nhiêu ngày  |          |
| d_l2_hour                  | varchar(256)             | staging.eros_dms                                         | thời điểm lên L2 là giờ nào trong ngày             | 2, 4     |
| d_l2_block                 | varchar(256)             | staging.eros_dms                                         | thời điểm lên L2 thuộc ca nào                      | 3, 4     |
| d_l2_day                   | date                     | staging.eros_dms                                         | thời điểm lên L2 là ngày nào                       | 4        |
| d_l2_week                  | int4                     | staging.eros_dms                                         | thời điểm lên L2 là tuần nào trong năm             | 4        |
| d_l2_month                 | int4                     | staging.eros_dms                                         | thời điểm lên L2 là tháng nào trong năm            | 4        |
| d_l2_year                  | int4                     | staging.eros_dms                                         | thời điểm lên L2 là năm nào                        | 4        |
| d_l2_quater                | int4                     | staging.eros_dms                                         | thời điểm lên L2 là quý nào trong năm              | 4        |
| d_l2_day_of_week           | int4                     | staging.eros_dms                                         | thời điểm lên L2 là ngày nào trong tuần            | 4        |
| d_l2_day_of_month          | int4                     | staging.eros_dms                                         | thời điểm lên L2 là ngày nào trong tháng           | 4        |
| d_l2_day_of_year           | int4                     | staging.eros_dms                                         | thời điểm lên L2 là ngày nào trong năm             | 4        |
| d_l2_fulltime              | timestamp                | staging.eros_dms                                         | thời điểm lên L2                                   | 4        |
| d_l2_date_t_since_order_at | int8                     | staging.eros_dms                                         | thời điểm lên L2 cách ngày đăng ký bao nhiêu ngày  | 4        |
| d_l3a_hour                 | varchar(256)             | staging.eros_dms                                         | thời điểm lên L3A là giờ nào trong ngày            | 2, 5     |
| d_l3a_block                | varchar(256)             | staging.eros_dms                                         | thời điểm lên L3A thuộc ca nào                     | 3, 5     |
| d_l3a_day                  | date                     | staging.eros_dms                                         | thời điểm lên L3A là ngày nào                      | 5        |
| d_l3a_week                 | int4                     | staging.eros_dms                                         | thời điểm lên L3A là tuần nào trong năm            | 5        |
| d_l3a_month                | int4                     | staging.eros_dms                                         | thời điểm lên L3A là tháng nào trong năm           | 5        |
| d_l3a_year                 | int4                     | staging.eros_dms                                         | thời điểm lên L3A là năm nào                       | 5        |
| d_l3a_quater               | int4                     | staging.eros_dms                                         | thời điểm lên L3A là quý nào trong năm             | 5        |
| d_l3a_day_of_week          | int4                     | staging.eros_dms                                         | thời điểm lên L3A là ngày nào trong tuần           | 5        |
| d_l3a_day_of_month         | int4                     | staging.eros_dms                                         | thời điểm lên L3A là ngày nào trong tháng          | 5        |
| d_l3a_day_of_year          | int4                     | staging.eros_dms                                         | thời điểm lên L3A là ngày nào trong năm            | 5        |
| d_l3a_fulltime             | timestamp                | staging.eros_dms                                         | thời điểm lên L3A                                  | 5        |
| d_l3_date_t_since_order_at | int8                     | staging.eros_dms                                         | thời điểm lên L3A cách ngày đăng ký bao nhiêu ngày | 5        |
| d_l4a_hour                 | varchar(256)             | staging.eros_dms                                         | thời điểm lên L4A là giờ nào trong ngày            | 2, 6     |
| d_l4a_block                | varchar(256)             | staging.eros_dms                                         | thời điểm lên L4A thuộc ca nào                     | 3, 6     |
| d_l4a_day                  | date                     | staging.eros_dms                                         | thời điểm lên L4A là ngày nào                      | 6        |
| d_l4a_week                 | int4                     | staging.eros_dms                                         | thời điểm lên L4A là tuần nào trong năm            | 6        |
| d_l4a_month                | int4                     | staging.eros_dms                                         | thời điểm lên L4A là tháng nào trong năm           | 6        |
| d_l4a_year                 | int4                     | staging.eros_dms                                         | thời điểm lên L4A là năm nào                       | 6        |
| d_l4a_quater               | int4                     | staging.eros_dms                                         | thời điểm lên L4A là quý nào trong năm             | 6        |
| d_l4a_day_of_week          | int4                     | staging.eros_dms                                         | thời điểm lên L4A là ngày nào trong tuần           | 6        |
| d_l4a_day_of_month         | int4                     | staging.eros_dms                                         | thời điểm lên L4A là ngày nào trong tháng          | 6        |
| d_l4a_day_of_year          | int4                     | staging.eros_dms                                         | thời điểm lên L4A là ngày nào trong năm            | 6        |
| d_l4a_fulltime             | timestamp                | staging.eros_dms                                         | thời điểm lên L4A                                  | 6        |
| d_l4_date_t_since_order_at | int8                     | staging.eros_dms                                         | thời điểm lên L4A cách ngày đăng ký bao nhiêu ngày | 6        |
| d_l7a_hour                 | varchar(256)             | staging.eros_dms                                         | thời điểm lên L7A là giờ nào trong ngày            | 2, 7     |
| d_l7a_block                | varchar(256)             | staging.eros_dms                                         | thời điểm lên L7A thuộc ca nào                     | 3, 7     |
| d_l7a_day                  | date                     | staging.eros_dms                                         | thời điểm lên L7A là ngày nào                      | 7        |
| d_l7a_week                 | int4                     | staging.eros_dms                                         | thời điểm lên L7A là tuần nào trong năm            | 7        |
| d_l7a_month                | int4                     | staging.eros_dms                                         | thời điểm lên L7A là tháng nào trong năm           | 7        |
| d_l7a_year                 | int4                     | staging.eros_dms                                         | thời điểm lên L7A là năm nào                       | 7        |
| d_l7a_quater               | int4                     | staging.eros_dms                                         | thời điểm lên L7A là quý nào trong năm             | 7        |
| d_l7a_day_of_week          | int4                     | staging.eros_dms                                         | thời điểm lên L7A là ngày nào trong tuần           | 7        |
| d_l7a_day_of_month         | int4                     | staging.eros_dms                                         | thời điểm lên L7A là ngày nào trong tháng          | 7        |
| d_l7a_day_of_year          | int4                     | staging.eros_dms                                         | thời điểm lên L7A là ngày nào trong năm            | 7        |
| d_l7a_fulltime             | timestamp                | staging.eros_dms                                         | thời điểm lên L7A                                  | 7        |
| d_l7_date_t_since_order_at | int8                     | staging.eros_dms                                         | thời điểm lên L7A cách ngày đăng ký bao nhiêu ngày | 7        |
| d_l8a_hour                 | varchar(256)             | staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses | thời điểm lên L8A là giờ nào trong ngày            | 2, 8, 9  |
| d_l8a_block                | varchar(256)             | staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses | thời điểm lên L8A thuộc ca nào                     | 3, 8, 9  |
| d_l8a_day                  | date                     | staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses | thời điểm lên L8A là ngày nào                      | 8, 9     |
| d_l8a_week                 | int4                     | staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses | thời điểm lên L8A là tuần nào trong năm            | 8, 9     |
| d_l8a_month                | int4                     | staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses | thời điểm lên L8A là tháng nào trong năm           | 8, 9     |
| d_l8a_year                 | int4                     | staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses | thời điểm lên L8A là năm nào                       | 8, 9     |
| d_l8a_quater               | int4                     | staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses | thời điểm lên L8A là quý nào trong năm             | 8, 9     |
| d_l8a_day_of_week          | int4                     | staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses | thời điểm lên L8A là ngày nào trong tuần           | 8, 9     |
| d_l8a_day_of_month         | int4                     | staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses | thời điểm lên L8A là ngày nào trong tháng          | 8, 9     |
| d_l8a_day_of_year          | int4                     | staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses | thời điểm lên L8A là ngày nào trong năm            | 8, 9     |
| d_l8a_fulltime             | timestamp                | staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses | thời điểm lên L8A                                  | 8, 9     |
| d_l8_date_t_since_order_at | int8                     | staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses | thời điểm lên L8A cách ngày đăng ký bao nhiêu ngày | 8, 9     |
| d_bifrost_l8_updated_at    | timestamp                | staging.eros_dms<br>staging.bifrost_dms<br>lake.bcourses | thời điểm lên L8 tại hệ thống thanh toán           |          |

**Note:**

1. *id order_date_dms* được định nghĩa là id của *eros_dms*. Khi không tìm thấy id từ bảng đó sẽ lấy transaction_course_id của *staging.bifrost_dms* với cấu trúc:   *'dir_' + lake.transactions.buyer_id + lake.bcourses.id*

2. format output: *hour + '_' + (hour+1)* , với *hour* là thời điểm quy đổi ra giờ nào trong ngày

3. format output: *'0h - 12h', '12h - 18h', '18h - 24h'*  , khi thời điểm quy đổi ra giờ trong ngày nằm trong khoảng thời gian tương ứng

4. lấy thời điểm nhỏ nhất của lịch sử đơn hàng tại L2 hoặc L2X

5. lấy thời điểm nhỏ nhất của lịch sử đơn hàng tại L3A hoặc L3B

6. lấy thời điểm nhỏ nhất của lịch sử đơn hàng tại L4A hoặc L4B

7. lấy thời điểm nhỏ nhất của lịch sử đơn hàng tại L7A hoặc L7B

8. lấy thời điểm nhỏ nhất của lịch sử đơn hàng tại L8A hoặc L8B

9. khi không tìm được thông tin trên contact c3 hoặc đơn hàng sẽ lấy thông tin trên *staging.bifrost_dms*
