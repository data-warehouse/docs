/* warehouse.order_sale_dms */

WITH
history_orders_ranked AS (
    SELECT
        hr.order_id                                                                                       AS order_id,
        hr.level                                                                                          AS level,
        hr.reason_not_buy                                                                                 AS reason_not_buy,
        tl.email                                                                                          AS email,
        hr.call_status                                                                                    AS call_status,
        CASE WHEN hr.level IN ('L1')         THEN hr.reason_not_buy END                                   AS sale_reason_not_l1,
        CASE WHEN hr.level IN ('L2' , 'L2X') THEN hr.reason_not_buy END                                   AS sale_reason_not_l2,
        CASE WHEN hr.level IN ('L3A', 'L3B') THEN hr.reason_not_buy END                                   AS sale_reason_not_l3,
        CASE WHEN hr.source NOT IN ('GAMBIT', 'BIFROST') THEN hr.call_status END                          AS sale_last_call_status_sale,
        CASE WHEN hr.source IN ('GAMBIT', 'BIFROST') THEN hr.call_status END                              AS sale_last_care_status_system,
        CASE WHEN hr.level IN ('L1')         THEN tl.email END                                            AS sale_in_l1,
        CASE WHEN hr.level IN ('L2' , 'L2X') THEN tl.email END                                            AS sale_in_l2,
        CASE WHEN hr.level IN ('L3A', 'L3B') THEN tl.email END                                            AS sale_in_l3,
        CASE WHEN hr.level IN ('L4A', 'L4B') THEN tl.email END                                            AS sale_in_l4,
        CASE WHEN hr.level IN ('L7A', 'L7B') THEN tl.email END                                            AS sale_in_l7,
        CASE WHEN hr.level IN ('L8A', 'L8B') THEN tl.email END                                            AS sale_in_l8,
        ROW_NUMBER() OVER (PARTITION BY hr.order_id, SUBSTRING(hr.level,1,2) ORDER BY hr.created_at ASC)  AS ranked,
        ROW_NUMBER() OVER (PARTITION BY hr.order_id, SUBSTRING(hr.level,1,2) ORDER BY hr.created_at DESC) AS ranked2
    FROM lake.history_orders AS hr
    LEFT JOIN lake.telesellers AS tl ON tl._id = hr.staff_id
)
SELECT
    morders._id                                                                                           AS id,
    morders.care_status                                                                                   AS sale_status,
    MAX(hr1.sale_reason_not_l1)                                                                           AS sale_reason_not_l1,
    MAX(hr1.sale_reason_not_l2)                                                                           AS sale_reason_not_l2,
    MAX(hr1.sale_reason_not_l3)                                                                           AS sale_reason_not_l3,
    MAX(morders.reason_not_buy)                                                                           AS sale_last_reason_not_buy,
    MAX(hr2.sale_last_call_status_sale)                                                                   AS sale_last_call_status_sale,
    MAX(morders.result)                                                                                   AS sale_last_care_status_sale,
    MAX(hr2.sale_last_care_status_system)                                                                 AS sale_last_care_status_system,
    MAX(hr1.sale_in_l1)                                                                                   AS sale_in_l1,
    MAX(hr1.sale_in_l2)                                                                                   AS sale_in_l2,
    MAX(hr1.sale_in_l3)                                                                                   AS sale_in_l3,
    MAX(hr1.sale_in_l4)                                                                                   AS sale_in_l4,
    MAX(hr1.sale_in_l7)                                                                                   AS sale_in_l7,
    MAX(hr1.sale_in_l8)                                                                                   AS sale_in_l8,
    MAX(hr2.email)                                                                                        AS telemar,
    COALESCE(MAX(hr3.email), MAX(hr2.email))                                                              AS last_telemar,
    MAX(hr4.email)                                                                                        AS sale_owner
FROM lake.morders
LEFT JOIN history_orders_ranked AS hr1 ON hr1.order_id = morders._id
    AND hr1.ranked = 1
LEFT JOIN history_orders_ranked AS hr2 ON hr2.order_id = morders._id
    AND hr2.level IN ('L1', 'L2', 'L2X')
    AND hr2.email NOT IN ('inventory@edumall.vn','async.autophone@edumall.vn', 'autophone@edumall.vn', 'errors.autophone@edumall.vn', 'inventory.autophone@edumall.vn', 'secrets.autophone@edumall.vn')
    AND hr2.ranked2 = 1
LEFT JOIN history_orders_ranked AS hr3 ON hr3.order_id = morders._id
    AND hr3.level IN ('L3A','L3B')
    AND hr3.email NOT IN ('inventory@edumall.vn','async.autophone@edumall.vn', 'autophone@edumall.vn', 'errors.autophone@edumall.vn', 'inventory.autophone@edumall.vn', 'secrets.autophone@edumall.vn')
    AND hr3.ranked = 1
LEFT JOIN lake.telesellers AS hr4 ON hr4._id = morders.staff_id
WHERE morders.flag_deleted IS NULL OR flag_deleted <> TRUE
GROUP BY morders._id, morders.care_status