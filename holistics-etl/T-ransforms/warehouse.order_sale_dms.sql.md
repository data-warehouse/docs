# [warehouse.order_sale_dms](../holistics-etl/T-ransforms/warehouse.order_sale_dms.sql)

| **Field**                    | **Data Type**            | **Table Lake**                                          | **Description**                                | **Note** |
| ---------------------------- | ------------------------ | ------------------------------------------------------- | ---------------------------------------------- | -------- |
| id                           | varchar(1020) (NOT NULL) | lake.morders                                            | id order_sale_dms                              | 1        |
| sale_status                  | varchar(1020)            | lake.morders<br>lake.history_orders<br>lake.telesellers | trạng thái đơn hàng                            |          |
| sale_reason_not_l1           | varchar(8332)            | lake.morders<br>lake.history_orders<br>lake.telesellers | lý do không mua ở L1                           | 2        |
| sale_reason_not_l2           | varchar(8332)            | lake.morders<br>lake.history_orders<br>lake.telesellers | lý do không mua ở L2                           | 2        |
| sale_reason_not_l3           | varchar(8332)            | lake.morders<br>lake.history_orders<br>lake.telesellers | lý do không mua ở L3                           | 2        |
| sale_last_reason_not_buy     | varchar(6000)            | lake.morders<br>lake.history_orders<br>lake.telesellers | lý do cuối cùng không mua                      |          |
| sale_last_call_status_sale   | varchar(1020)            | lake.morders<br>lake.history_orders<br>lake.telesellers | trạng thái cuộc gọi lần cuối                   |          |
| sale_last_care_status_sale   | varchar(8332)            | lake.morders<br>lake.history_orders<br>lake.telesellers | trạng thái chăm sóc lần cuối                   | 3, 4     |
| sale_last_care_status_system | varchar(8332)            | lake.morders<br>lake.history_orders<br>lake.telesellers | trạng thái lần cuối của hệ thống               |          |
| sale_in_l1                   | varchar(1020)            | lake.morders<br>lake.history_orders<br>lake.telesellers | email nhân viên sale chăm sóc tại L1           | 2, 4     |
| sale_in_l2                   | varchar(1020)            | lake.morders<br>lake.history_orders<br>lake.telesellers | email nhân viên sale chăm sóc tại L2           | 2, 4     |
| sale_in_l3                   | varchar(1020)            | lake.morders<br>lake.history_orders<br>lake.telesellers | email nhân viên sale chăm sóc tại L3           | 2, 4     |
| sale_in_l4                   | varchar(1020)            | lake.morders<br>lake.history_orders<br>lake.telesellers | email nhân viên sale chăm sóc tại L4           | 2, 4     |
| sale_in_l7                   | varchar(1020)            | lake.morders<br>lake.history_orders<br>lake.telesellers | email nhân viên sale chăm sóc tại L7           | 2, 4     |
| sale_in_l8                   | varchar(1020)            | lake.morders<br>lake.history_orders<br>lake.telesellers | email nhân viên sale chăm sóc tại L8           | 2, 4     |
| telemar                      | varchar(1020)            | lake.morders<br>lake.history_orders<br>lake.telesellers | email telemar chăm sóc                         | 5, 6     |
| last_telemar                 | varchar(1020)            | lake.morders<br>lake.history_orders<br>lake.telesellers | email telemar cuối cùng chăm sóc               | 5, 7     |
| sale_owner                   | varchar(1020)            | lake.morders<br>lake.telesellers                        | email nhân viên sale gắn với đơn hàng hiện tại |          |

**Note:**

1. *id order_sale_dms* được định nghĩa là id của *lake.morders*
2. L2 = L2 + L2X, L3 = L3A + L3B, L4 = L4A + L4B, L7 = L7A + L7B, L8 = L8A +L8B
3. *trạng thái chăm sóc lần cuối* là *trạng thái cuộc gọi lần cuối* hoặc *lý do cuối cùng không mua*
4. thông tin lấy từ lịch sử đơn hàng (bảng *lake.history_orders*) không bao gồm những nguồn (*lake.history_orders.source*) từ hệ thống psmo (GAMBIT) hoặc hệ thống thanh toán (BIFROST)
5. *email telemar* được định nghĩa là email nhân viên chăm sóc đơn hàng từ L1 đến L3, không bao gồm những email được định nghĩa để chứa kho contacts c3 hoặc email autophone, bao gồm: *inventory@edumall.vn ,async.autophone@edumall.vn, autophone@edumall.vn, errors.autophone@edumall.vn, inventory.autophone@edumall.vn, secrets.autophone@edumall.vn*
6. *telemar chăm sóc* được định nghĩa là nhân viên đầu tiên chăm sóc đơn hàng từ L1 đến L2/L2X
7. *telemar cuối cùng chăm sóc* được định nghĩa là nhân viên đầu tiên chăm sóc đơn hàng tại L3A/L3B hoặc nhân viên cuối cùng chăm sóc đơn hàng từ L1 đến L2/L2X