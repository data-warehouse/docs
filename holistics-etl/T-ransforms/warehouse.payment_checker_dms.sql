/* warehouse.payment_checker_dms */

WITH
xt_ranked AS (
    SELECT
        xcheck_transactions.*,
        COALESCE(gorders.siren_id, xcheck_transactions.siren_id) AS g_siren_id,
        ROW_NUMBER() OVER (PARTITION BY xcheck_transactions.siren_id ORDER BY xcheck_transactions.price DESC) AS ranked
    FROM lake.xcheck_transactions
    LEFT JOIN lake.gorders ON gorders.lading_code = xcheck_transactions.transfer_code
)
(SELECT
    xt_ranked.transfer_code AS transfer_code,
    xt_ranked.student AS student,
    xt_ranked.source_revshare AS source_revshare,
    xt_ranked.source AS source,
    xt_ranked.g_siren_id AS siren_id, /* PRIMARY KEY */
    xt_ranked.seller AS seller,
    CAST(xt_ranked.price AS INTEGER) AS price,
    CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, xt_ranked.money_arrival_date)) AS money_arrival_date,
    xt_ranked.method AS method,
    CAST(xt_ranked.fee AS REAL) AS fee,
    xt_ranked.course_name AS course_name,
    xt_ranked.course_code AS course_code
FROM xt_ranked
WHERE xt_ranked.g_siren_id IS NOT NULL AND xt_ranked.ranked = 1
)
