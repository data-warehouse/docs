# [warehouse.payment_checker_dms](../holistics-etl/T-ransforms/warehouse.payment_checker_dms.sql)

| **Field**          | **Data Type** | **Table Lake**                           | **Description**            | **Note** |
| ------------------ | ------------- | ---------------------------------------- | -------------------------- | -------- |
| transfer_code      | varchar(1020) | lake.xcheck_transactions                 | mã vận đơn/ mã thanh toán  | 3        |
| student            | varchar(1020) | lake.xcheck_transactions                 | tên                        | 3        |
| source_revshare    | varchar(1020) | lake.xcheck_transactions                 | nguồn revshare             | 3        |
| source             | varchar(1020) | lake.xcheck_transactions                 | nguồn đơn                  | 3        |
| siren_id           | varchar(1020) | lake.xcheck_transactions<br>lake.gorders | mã đơn hàng/ mã contact c3 | 1, 3     |
| seller             | varchar(1020) | lake.xcheck_transactions                 | người bán                  | 3        |
| price              | int4          | lake.xcheck_transactions                 | giá bán                    | 3        |
| money_arrival_date | timestamp     | lake.xcheck_transactions                 | ngày thanh toán            | 2, 3     |
| method             | varchar(1020) | lake.xcheck_transactions                 | hình thức thanh toán       | 3        |
| fee                | float4        | lake.xcheck_transactions                 | phí                        | 3        |
| course_name        | varchar(1020) | lake.xcheck_transactions                 | tên khoá học               | 3        |
| course_code        | varchar(1020) | lake.xcheck_transactions                 | mã khoá học                | 3        |

**Note:**

1. *siren_id* được định nghĩa là *siren_id* của bảng *lake.gorders* hoặc *lake.xcheck_transactions*
2. timezone : *Asia/Ho_Chi_Minh*
3. ưu tiên lấy những bản ghi thông tin đối soát có giá

