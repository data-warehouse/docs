/* warehouse.payment_dms */

-- DONE: Upgrade bifrostDB to mysql 5.7+ (NamVH), and extract/import orderer_email
-- Update by TrungND 9:00 AM 11/10/2017
-- WITH edm_payment AS (
--   SELECT
--     CONVERT_TIMEZONE('Asia/Ho_Chi_Minh',CONVERT(TIMESTAMP, edumall_payments.activate_at)) AS activate_at,
--     transaction_details._transaction_id,
--     GREATEST(edumall_payments.updated_at,transaction_details.updated_at) AS updated_at
--   FROM lake.edumall_payments
--   LEFT JOIN lake.transaction_details ON transaction_details.payment_id = edumall_payments._id
-- )
-- staging_payment_dms AS (
SELECT
    DISTINCT CAST(transactions.id AS VARCHAR) AS payment_id,
    transactions.method AS payment_method,
    transactions.status AS payment_status,
    -- (CASE WHEN COUNT(bcourses.id) <= 1 THEN MIN(CAST(bcourses.price AS REAL))  ELSE CAST(transactions.price AS REAL) END) AS promotion_price,
    CAST(transactions.price AS REAL) AS promotion_price,
    -- transactions.orderer_email AS payment_creater,
    -- CAST(MIN(bcourses.price) AS REAL) AS promotion_price,
    (CASE WHEN COUNT(bcourses.id) > 1 THEN transactions.coupon_code ELSE MIN(bcourses.coupon_code)     END) AS payment_coupon,
    (CASE WHEN COUNT(bcourses.id) > 1 THEN NULL                     ELSE MIN(bcourses.revshare)        END) AS revshare,
    (CASE WHEN COUNT(bcourses.id) > 1 THEN '<various>'              ELSE MIN(bcourses.source_revshare) END) AS revshare_source,
    transactions.note AS note,
    transactions.seller AS seller,
    transactions.source_url AS source_url
FROM lake.transactions
LEFT JOIN lake.bcourses ON bcourses._transaction_id = transactions.id
-- OR transactions.edumall_payment_id = edumall_payments._id
GROUP BY transactions.id,
         transactions.method,
         transactions.status,
         transactions.price,
        -- transactions.orderer_email,
         -- transactions.combo,
         transactions.coupon_code,
         transactions.note,
         transactions.seller,
         transactions.source_url
         -- transactions.updated_at,
         -- bcourses.updated_at
-- )
-- SELECT *
-- FROM staging_payment_dms
-- WHERE [[ updated_at > {{max_value}} ]]