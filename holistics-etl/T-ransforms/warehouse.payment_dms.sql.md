# [warehouse.payment_dms](../holistics-etl/T-ransforms/warehouse.payment_dms.sql)

| **Field**       | **Data Type**            | **Table Lake**                                     | **Description**                        | **Note** |
| --------------- | ------------------------ | -------------------------------------------------- | -------------------------------------- | -------- |
| payment_id      | varchar(1020) (NOT NULL) | lake.transactions                                  | mã thanh toán                          |          |
| payment_method  | varchar(1020)            | lake.transactions                                  | hình thức thanh toán                   |          |
| payment_status  | varchar(1020)            | lake.transactions                                  | trạng thái thanh toán                  |          |
| promotion_price | float4                   | lake.transactions                                  | giá khuyến mãi                         |          |
| payment_jf_id   | varchar(1020)            | lake.transactions<br>lake.bcourses                 | mã payment lấy trên hệ thống Jackfruit | 1        |
| payment_coupon  | varchar(1020)            | lake.transactions<br>lake.bcourses                 | mã coupon                              |          |
| revshare        | float4                   | lake.transactions<br>lake.bcourses                 | revshare                               | 2        |
| revshare_source | varchar(1020)            | lake.transactions<br>lake.bcourses                 | nguồn revshare                         | 1        |
| course_code     | varchar(1020)            | lake.transactions<br>lake.bcourses                 | mã khoá học                            |          |
| course_name     | varchar(1020)            | lake.transactions<br>lake.bcourses                 | tên khoá học                           |          |
| note            | varchar(8332)            | lake.transactions<br>lake.bcourses                 | thông tin thanh toán bổ sung           |          |
| seller          | varchar(1020)            | lake.transactions<br>lake.bcourses                 | người bán                              |          |
| source_url      | varchar(1020)            | lake.transactions                                  | nguồn url tại thanh toán               |          |
| activate_at     | timestamp                | lake.edumall_payments<br/>lake.transaction_details | thời gian kích hoạt khóa học           |          |

**Note:**

1. output là *<various>* khi khóa học là combo
2. ourput là NULL khi khóa học là combo