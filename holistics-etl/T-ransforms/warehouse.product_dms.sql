/* warehouse.product_dms */

-- WITH staging_product_dms AS (
(SELECT
    COALESCE(contactc3s.type,        morders.type)       AS pd_type,
    COALESCE(contactc3s.target,      morders.target)     AS pd_target,
    COALESCE(contactc3s.strategy,    morders.strategy)   AS pd_strategy,
    COALESCE(contactc3s.source,      morders.source)     AS pd_source,
    COALESCE(contactc3s.source_url,  morders.source_url) AS pd_source_url,
    --CANCELLED: AS pd_combo
    --CANCELLED: contactc3s.course_id    AS pd_course_id,
    --CANCELLED: contactc3s.course_name  AS pd_course_name,
    COALESCE(contactc3s._id,         morders._id)        AS pd_product_id, /* PRIMARY KEY */
    (CASE WHEN landingpage.domain IS NOT NULL
        THEN (landingpage.domain || '.' || landingpage.pedia_domain) END) AS pd_landing_page,
    --CANCELLED: AS pd_course_code,
    COALESCE(contactc3s.status,         morders.status)  AS pd_status,
    COALESCE(contactc3s.coupon, morders.coupon_code)     AS pd_coupon,
    contactc3s.referrer_url                              AS pd_referrer_url
    -- GREATEST(contactc3s.updated_at, landingpage.updated_at, morders.updated_at) AS updated_at
FROM lake.contactc3s
LEFT       JOIN lake.landingpage  ON extract_hostname(contactc3s.source_url) = (landingpage.domain || '.' || landingpage.pedia_domain)
FULL OUTER JOIN lake.morders ON morders._id = contactc3s._id
) UNION ALL (
-- Direct (Go straight to system without advertising (nami) or sale convert (eros))
SELECT DISTINCT
    'notavailable' AS pd_type,
    (CASE WHEN MAX(tr.combo) IS NOT NULL THEN 'combo' ELSE 'course' END) AS pd_target,
    'notavailable' AS pd_strategy,
    MAX(tr.source) AS pd_source,
    MAX(tr.source_url) AS pd_source_url,
    --CANCELLED: AS pd_combo
    --CANCELLED: contactc3s.course_id    AS pd_course_id,
    --CANCELLED: contactc3s.course_name  AS pd_course_name,
    tr.transaction_course_id AS pd_product_id, /* PRIMARY KEY */
    'notavailable' AS pd_landing_page,
    --CANCELLED: AS pd_course_code,
    'notavailable' AS pd_status,
    -- (CASE WHEN MAX(tr.combo) IS NOT NULL THEN MAX(tr.coupon_code) ELSE f_distinct_listagg(LISTAGG(bcourses.coupon_code, ',')) END) AS pd_coupon
    'notavailable' AS pd_coupon
    'notavailable' AS pd_referrer_url
    -- GREATEST(tr.updated_at, bcourses.updated_at) AS updated_at
FROM staging.bifrost_dms AS tr
LEFT JOIN lake.bcourses ON bcourses._transaction_id = tr.id
WHERE tr.ranked = 1
GROUP BY tr.transaction_course_id, tr.updated_at, bcourses.updated_at)
-- )
-- SELECT *
-- FROM staging_product_dms
-- WHERE [[ updated_at > {{max_value}} ]]
