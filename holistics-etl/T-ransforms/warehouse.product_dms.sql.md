# [warehouse.product_dms](../holistics-etl/T-ransforms/warehouse.product_dms.sql)

| **Field**       | **Data Type**            | **Table Lake**                                               | **Description**                    | **Note** |
| --------------- | ------------------------ | ------------------------------------------------------------ | ---------------------------------- | -------- |
| pd_type         | varchar(1020)            | lake.contactc3s<br>lake.morders                              | loại product                       | 1        |
| pd_target       | varchar(1020)            | lake.contactc3s<br>lake.morders<br>staging.bifrost_dms<br>lake.bcourses | đối tượng                          | 1, 4     |
| pd_strategy     | varchar(1020)            | lake.contactc3s<br>lake.morders                              | chiến lược                         | 1        |
| pd_source       | varchar(1020)            | lake.contactc3s<br>lake.morders<br>staging.bifrost_dms<br>lake.bcourses | nguồn                              | 1, 4     |
| pd_source_url   | varchar(8332)            | lake.contactc3s<br>lake.morders<br>staging.bifrost_dms<br>lake.bcourses | nguồn url                          | 1, 4     |
| pd_product_id   | varchar(1020) (NOT NULL) | lake.contactc3s<br>staging.bifrost_dms<br>lake.bcourses      | product id                         | 1, 3, 4  |
| pd_landing_page | varchar(2041)            | lake.contactc3s<br>lake.morders<br>lake.landingpage          | url landingpage                    | 1, 4     |
| pd_status       | varchar(1020)            | lake.contactc3s                                              | trạng thái contacts c3             |          |
| pd_coupon       | varchar(1020)            | lake.contactc3s<br>lake.morders<br>staging.bifrost_dms<br>lake.bcourses | mã coupon                          | 1, 4     |
| updated_at      | timestamp (NOT NULL)     | lake.contactc3s<br>lake.morders<br>lake.landingpage<br>staging.bifrost_dms<br>lake.bcourses | thời điểm cập nhật của product_dms | 1, 2, 4  |

**Note:**

1. cùng một trường thông tin nếu không lấy được trên bảng *lake.contactc3s* sẽ lấy trên bảng *lake.morders*

2. thời điểm cập nhật lần cuối các bản ghi của bảng *warehouse.product_dms*  (dùng để thử nghiệm increamental transform)

3. *pd_product_id* được định nghĩa là id của bảng *lake.contactc3s* hoặc *lake.morders*. Khi không tìm thấy id từ 2 bảng đó sẽ lấy transaction_course_id của *staging.bifrost_dms* với cấu trúc:   *'dir_' + lake.transactions.buyer_id + lake.bcourses.id*

4. khi không tìm được thông tin trên contact c3 hoặc đơn hàng sẽ lấy thông tin trên *staging.bifrost_dms*