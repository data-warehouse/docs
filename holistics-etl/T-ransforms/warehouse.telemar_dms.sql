SELECT
    telesellers.name AS tm_name,
    telesellers.email AS tm_email,
    telesellers._id AS tm_id,
    telesellers.mobile AS tm_mobile,
    group_works.name AS tm_group,
    GREATEST(telesellers.updated_at,group_works.updated_at) AS updated_at
FROM lake.telesellers
LEFT JOIN lake.group_works
ON group_works._id  = telesellers.group_work_id
WHERE group_works.name IN ('Telemarketer', 'CTV Telesale')