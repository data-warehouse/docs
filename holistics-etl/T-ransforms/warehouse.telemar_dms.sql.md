# [warehouse.telemar_dms](../holistics-etl/T-ransforms/warehouse.telemar_dms.sql)

| **Field**  | **Data Type**            | **Table Lake**                       | **Description**                    | **Note** |
| ---------- | ------------------------ | ------------------------------------ | ---------------------------------- | -------- |
| tm_name    | varchar(1020)            | lake.telesellers                     | tên nhân viên                      | 2        |
| tm_email   | varchar(1020)            | lake.telesellers                     | email nhân viên                    | 2        |
| tm_id      | varchar(1020) (NOT NULL) | lake.telesellers                     | id telemar_dms                     | 1        |
| tm_mobile  | varchar(1020)            | lake.telesellers                     | số điện thoại nhân viên            | 2        |
| tm_group   | varchar(1020)            | lake.group_works                     | nhóm nhân viên                     |          |
| updated_at | timestamp                | lake.telesellers<br>lake.group_works | thời điểm cập nhật của telemar_dms | 3        |

**Note:**

1. *id telemar_dms* được định nghĩa là id của bảng *lake.telesellers*
2. thông tin telemar trên hệ thống eros, là những nhân viên nằm trong nhóm nhân viên (*lake.group_works.name*) *Telemarketer* hoặc *CTV Telesale*
3. thời điểm cập nhật lần cuối các bản ghi của bảng *warehouse.telemar_dms* (dùng để increamental transform dữ liệu)