WITH staging_telesale_dms AS (
  SELECT
      telesellers.name AS tl_name,
      telesellers.email AS tl_email,
      telesellers._id AS tl_id,
      telesellers.mobile AS tl_mobile,
      group_works.name AS tl_group,
      GREATEST(telesellers.updated_at,group_works.updated_at) AS updated_at
  FROM lake.telesellers
  LEFT JOIN lake.group_works
  ON group_works._id  = telesellers.group_work_id
)
SELECT
    *,
    CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', getdate()) AS transform_at
FROM staging_telesale_dms
WHERE [[ updated_at > {{max_value}} ]]