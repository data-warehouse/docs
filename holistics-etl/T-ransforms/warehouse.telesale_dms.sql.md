# [warehouse.telesale_dms](../holistics-etl/T-ransforms/warehouse.telesale_dms.sql)

| **Field**  | **Data Type**            | **Table Lake**                       | **Description**                     | **Note** |
| ---------- | ------------------------ | ------------------------------------ | ----------------------------------- | -------- |
| tm_name    | varchar(1020)            | lake.telesellers                     | tên nhân viên                       | 2        |
| tm_email   | varchar(1020)            | lake.telesellers                     | email nhân viên                     | 2        |
| tm_id      | varchar(1020) (NOT NULL) | lake.telesellers                     | id telesale_dms                     | 1        |
| tm_mobile  | varchar(1020)            | lake.telesellers                     | số điện thoại nhân viên             | 2        |
| tm_group   | varchar(1020)            | lake.group_works                     | nhóm nhân viên                      |          |
| updated_at | timestamp                | lake.telesellers<br>lake.group_works | thời điểm cập nhật của telesale_dms | 3        |

**Note:**

1. *id telesale_dms* được định nghĩa là id của bảng *lake.telesellers*
2. thông tin telesale trên hệ thống eros
3. thời điểm cập nhật lần cuối các bản ghi của bảng *warehouse.telesale_dms* (dùng để increamental transform dữ liệu)