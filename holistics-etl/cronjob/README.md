# RedEdumall CronJob

:warning: DEPRECATED: This repo moved to [tcs/red_edumall](https://git.edumall.io/tcs/red_edumall)

Kubernetes CronJob that use [red_edumall gem](http://git.edumall.vn/tcs/red_edumall) for control ETL processes.

## Deployment

**Make sure that you make a view over YAML files and fill in your `holistics token, redshift configuration, modify cronjob schedule time,... etc` before deploy**

1. Build and Push a docker image that contain `red_edumall` gem to your registry

```
docker build -t 350339004156.dkr.ecr.ap-southeast-1.amazonaws.com/red_edumall_cron:latest .
docker push 350339004156.dkr.ecr.ap-southeast-1.amazonaws.com/red_edumall_cron:latest
```

2. Deploy CronJob to Kubernetes

```
kubectl create namespace data-warehouse
kubectl apply -f red-edumall-configmap.yaml
kubectl apply -f holistics-configmap.yaml
kubectl apply -f red-edumall-cron.yaml
```

## Describes

* `red_edumall_cron` and `red_edumall_cron.pub` is used for pulling red_edumall gem from private gitlab, by add public key as deploy key

* 2 configmap files is config files of **Holistics CLI** and **RedEdumall gem**, by default it located in `~/.holistics.yml` and `~/.red_edumall/config`. Read more: https://docs.holistics.io/holistics-cli/ and http://git.edumall.vn/tcs/red_edumall