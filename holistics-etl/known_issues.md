# Known Issues

##### Bug Nami: 1 con C3 được tính 2 lần ở 2 ads khác nhau

_1. proof_

```
SELECT
  c3copies.id AS c3copies_id,
  ad_bydates.*
FROM ad_bydates
JOIN ads ON ads.id = ad_bydates.ad_id
JOIN c3copies ON c3copies.utm_medium = ads.utm_medium AND DATE(ad_bydates.bydate) = DATE(c3copies.date)
WHERE ad_bydates.bydate = '2017-05-02' AND ad_bydates.ad_id IN (127498, 117398)
```

_2. Resolve_

Nami: join trường `utm_medium` dưới dạng BINARY để `exactly compare`

Datawarehouse: Về mặt số lượng sẽ ko bị ảnh hưởng, tuy nhiên nên join theo BINARY để đảm bảo c3 về chính xác ads. Tuy nhiên đối với Redshift không cần sửa (exactly compared by default)

##### Bug lake.provinces: bị dupplicate trường code

_1. proof_

```
SELECT
  code,
  COUNT(code)
FROM lake.provinces
GROUP BY code
HAVING COUNT(code) > 1
```

_2. Reason_

Nghi ngờ có thanh niên chọc ngoáy vào data lake

_3. Resolve_

Quản lý chặt tài khoản truy cập vào data lake

Refresh lại data provinces mỗi khi transform ((red_edumall)[https://git.edumall.io/tcs/red_edumall])

##### Bug nami: 1 marketers 1 thời điểm nằm ở 2 team khác nhau

_1. proof_

```
--SELECT * FROM warehouse.ads_dms WHERE warehouse.ads_dms.id = 334413

SELECT
    CAST(ad_bydates.id AS VARCHAR) AS id,
    marketers.id AS MKT_ID,
    marketers.email,
    group_users.user_id,
    ad_bydates.bydate,
  group_users.start_date,
  group_users.end_date
FROM lake.ad_bydates
LEFT JOIN lake.ads             ON ads.id                    = ad_bydates.ad_id
LEFT JOIN lake.ad_groups       ON ad_groups.id              = ads.ad_group_id
LEFT JOIN lake.campaigns       ON campaigns.id              = ad_groups.campaign_id
LEFT JOIN lake.ccusers         ON ccusers.channel_course_id = campaigns.channel_course_id
                                AND DATE(ccusers.date) = DATE(ad_bydates.bydate)
LEFT JOIN lake.marketers       ON marketers.id              = ccusers.user_id
LEFT JOIN lake.channel_courses ON channel_courses.id        = campaigns.channel_course_id
LEFT JOIN lake.channels        ON channels.id               = channel_courses.channel_id
LEFT JOIN lake.adaccounts      ON adaccounts.id             = campaigns.adaccount_id
--
LEFT JOIN lake.mar_teams_marketers AS group_users ON group_users.user_id = marketers.id
                                                    AND DATE(ad_bydates.bydate) >= DATE(group_users.start_date)
                                                    AND DATE(ad_bydates.bydate) < DATE(group_users.end_date)
--LEFT JOIN lake.mar_teams           AS groups      ON groups.id  = group_users.group_id
--LEFT JOIN lake.marketers           AS manager     ON manager.id = groups.manager_id
WHERE ad_bydates.id = 334413
```

_2. Reason_

Marketing vận hành sai logic

_3. Resolve_

Làm việc với marketing để giải quyết vđ này hoặc đưa ra phương án thay đổi logic dữ liệu
