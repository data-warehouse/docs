/*
-- UDFs: User-Defined-Functions
*/

/*
-- Extract hostname from provided url
-- Eg:
---- input: https://giaoducsom.edumall.vn/?utm_source=FA&utm_medium=FA_QuynhHTN.01_CV1_Huong&utm_campaign=QuynhHTN.01
---- return: giaoducsom.edumall.vn
*/
create or replace function extract_hostname (source_url varchar(2083))
returns varchar
immutable AS $$
    from urlparse import urlparse
    if source_url is None:
        return None
    o = urlparse(source_url)
    return o.hostname
$$ language plpythonu;

/*
-- Sanitize price string to integer value, auto multiply with 1000 when price lower 1000
-- Eg:
---- input: "299,000" or "299.000" or "299"
---- return: 299000
*/
create or replace function normalize_price (s varchar(1024))
returns int
immutable AS $$
    import string
    if s is None:
        return None
    all=string.maketrans('','')
    nodigs=all.translate(all, string.digits)
    s=s.translate(all, nodigs)
    s=s.strip()
    if s == '':
        return None
    ret = int(float(s))
    if ret < 1000:
        return ret * 1000
    else:
        return ret
$$ language plpythonu;

/*
-- Sanitize cost string to float value
-- Eg:
---- input: "45.1422" or "34" or "129,345"
---- return: 45.1422  or  34  or  129345
*/
create or replace function to_float (s varchar(255))
returns real
immutable AS $$
    import re
    from decimal import Decimal
    if s is None:
        return None
    non_decimal = re.compile(r'[^\d.]+')
    s = non_decimal.sub('', s)
    if s == '':
        return None
    return Decimal(s)
$$ language plpythonu;

/*
-- Calculate sale block based input timestamp by specify kind of block
-- Eg:
---- input: timestamp is `15:48:29 2017-04-01`
---- return: "15h - 16h" for "hour"
----        and "12h - 18h" for "block"
*/
create or replace function order_date_helper (t timestamp, part varchar(255))
returns varchar
immutable AS $$
    if t is None:
        return None
    if part == 'hour':
        return (str(t.hour) + 'h - ' + str(t.hour + 1) + 'h')
    elif part == 'block':
        if 0 <= t.hour < 12:
            return '0h - 12h'
        elif 12 <= t.hour < 18:
            return '12h - 18h'
        elif 18 <= t.hour:
            return '18h - 24h'
$$ language plpythonu;

/*
-- Distinct LISTAGG with Python UDF
-- Eg:
---- input: "kinh-doanh, kinh-doanh"
---- return: "kinh-doanh"
-- Source: https://discourse.looker.com/t/distinct-listagg-for-redshift-using-python-udfs/5128
*/
CREATE FUNCTION f_distinct_listagg (s varchar(max))
  RETURNS varchar(max)
IMMUTABLE
AS $$
  return ','.join(sorted(set(s.split(',')))) if s is not None else None
$$ LANGUAGE plpythonu;
