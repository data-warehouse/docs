/*
-- BEFORE TRANSFORM: Validate for data sources: to make sure source data don't break after T-ransform
*/

/*
-- Pass if count = 0
-- else Failue
*/

SELECT
  COUNT(*)
FROM lake.c3copies
LEFT JOIN lake.contactc3s ON contactc3s._id = c3copies.id
WHERE DATE(CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', CONVERT(TIMESTAMP, contactc3s.created_at))) <> DATE(c3copies.date)

/*
-- AFTER TRANSFORM: Validate transformed data don't break the concept
*/

/*
-- Make sure with or without dimension JOINS, the count still the same
*/

SELECT
    COUNT(*)
FROM warehouse.lead_facts
LEFT JOIN warehouse.product_dms         ON warehouse.product_dms.pd_product_id = warehouse.lead_facts.product_dm_id
LEFT JOIN warehouse.date_dms            ON warehouse.date_dms.id               = warehouse.lead_facts.date_dm_id
LEFT JOIN warehouse.telemar_dms         ON warehouse.telemar_dms.tm_id         = warehouse.lead_facts.telemar_dm_id
LEFT JOIN warehouse.telesale_dms        ON warehouse.telesale_dms.tl_id        = warehouse.lead_facts.telesale_dm_id
LEFT JOIN warehouse.ads_dms             ON warehouse.ads_dms.id                = warehouse.lead_facts.ads_dm_id
LEFT JOIN warehouse.customer_dms        ON warehouse.customer_dms.cd_id        = warehouse.lead_facts.customer_dm_id
LEFT JOIN warehouse.lading_dms          ON warehouse.lading_dms.lg_lading_id   = warehouse.lead_facts.lading_dm_id
LEFT JOIN warehouse.lading_employee_dms ON warehouse.lading_employee_dms.le_id = warehouse.lead_facts.lading_employee_dm_id
LEFT JOIN warehouse.level_dms           ON warehouse.level_dms.id              = warehouse.lead_facts.level_dm_id
LEFT JOIN warehouse.order_date_dms      ON warehouse.order_date_dms.id         = warehouse.lead_facts.order_date_dm_id
LEFT JOIN warehouse.order_sale_dms      ON warehouse.order_sale_dms.id         = warehouse.lead_facts.order_sale_dm_id
LEFT JOIN warehouse.lading_date_dms     ON warehouse.lading_date_dms.id        = warehouse.lead_facts.lading_date_dm_id
LEFT JOIN warehouse.payment_dms         ON warehouse.payment_dms.payment_id    = warehouse.lead_facts.payment_dm_id

/*
-- Pass if no records returned
-- else Failue
*/

SELECT
  id,
  COUNT(id)
FROM warehouse.ads_dms
GROUP BY id
HAVING COUNT(id) > 1
