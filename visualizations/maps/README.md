## Map data for visualizations

> :warning: We collect those visualizations from many sources, that include many LISENCES.
> 
> So, read it carefully before using or deploy to production. See lisence detail in each directories.

### Sources

1. www.gadm.org

### Converter

> Purpose: Convert many map data type (GeoPackage, Shape files, ..) into GeoJSON type, to compatible with Metabase

###### 1. Install `gdal` (gdal is included `ogr2ogr `, so that we can use ogr2ogr to convert)

> MacOSX

```
brew install gdal
```

> Linux

* Add Ubuntu GIS repo `https://launchpad.net/~ubuntugis/+archive/ubuntu/ppa`
* And install gdal: `sudo apt-get install gdal`

###### 2. Convert Shapefile into GeoJSON

```
ogr2ogr -f GeoJSON -t_srs crs:84 VNM_adm1.geojson VNM_adm1.shp
```
